// Copyright (C) 2019 Pasi Kaukinen

// This file is part of Siilinkarin tuulet.

// Siilinkarin tuulet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Siilinkarin tuulet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Siilinkarin tuulet.  If not, see <https://www.gnu.org/licenses/>.

import QtQuick
import QtQuick.Controls

Page {
    width: root.width
    height: root.height
    header: Label {
        id: label_header
        text: qsTr("TIETOJA")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    // FMI License
    Text {
        id: license
        textFormat: Text.RichText;
        text: "<style>a:link { color: '#B0B0B0'; }</style><b>" +
              qsTr("Siilinkarin tuulet") + " " + app.applicationVersion + "</b><p>&nbsp;<p>" +
              qsTr("Kotisohvan sovellukset tekee")+"<br>Pasi Kaukinen"+"<p>" +
              qsTr("Mittaustulokset tarjoaa Ilmatieteen laitos") +
              "<br><a href='http://creativecommons.org/licenses/by/4.0/deed.fi'>" +
              qsTr("Lisenssi") + "</a><p>" +
              "This application is released under <a href='http://www.gnu.org/licenses/gpl-3.0.html'>GPLv3 license</a> using Qt framework" + "<p>"+
              "The application is provided AS IS with NO WARRANTY OF ANY KIND" + "<p>" +
              "Source files can be found from bitbucket.org with keywords Kotisohva Siilinkari<p>";
        color: "#B0B0B0"
        width: parent.width*0.9
        height: isPortrait() ? parent.height * 0.12 : parent.height * 0.2
        anchors.top: parent.top
        anchors.topMargin: parent.height * 0.2
        anchors.left: parent.left
        anchors.leftMargin: parent.width*0.05
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignVCenter
        fontSizeMode: Text.Fit
        font.pixelSize: height*0.2
        onLinkActivated: Qt.openUrlExternally(link)
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    }
}




