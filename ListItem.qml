// Copyright (C) 2019 Pasi Kaukinen

// This file is part of Siilinkarin tuulet.

// Siilinkarin tuulet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Siilinkarin tuulet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Siilinkarin tuulet.  If not, see <https://www.gnu.org/licenses/>.

import QtQuick

Item {
    property alias text1: idtext1.text
    property alias color1: idtextd.color

    property alias text2: idtext2.text
    property alias color2: idtext2.color

    property var textd

    width: isPortrait() ? parent.width : parent.width * 0.5
    height: isPortrait() ? parent.height * 0.15 : parent.height * 0.30


    function toCustom(t)
    {
        var d = new Date()
        var s2 = t.toTimeString().substring(0,5)
        return s2
    }

    Text {
       id: idtext1
       text: ""
       color: dark() ? "#B0B0B0" : "#606060"
       width: parent.width * 0.55
       height: parent.height * 0.3
       anchors.top: parent.top
       anchors.left: parent.left
       horizontalAlignment: Text.AlignRight
       verticalAlignment: Text.AlignBottom
       fontSizeMode: Text.Fit
       font.pixelSize: height * 0.8
    }

    Text {
       id: idtextd
       color: "white"
       text: parent.textd
       width: parent.width * 0.3
       height: parent.height * 0.3
       anchors.top: parent.top
       anchors.right: parent.right
       anchors.rightMargin: parent.width * 0.2
       horizontalAlignment: Text.AlignRight
       verticalAlignment: Text.AlignBottom
       fontSizeMode: Text.Fit
       font.pixelSize: height * 0.8
    }

    Text {
       id: idtext2
       text: ""
       width: parent.width
       height: parent.height * 0.7
       anchors.top: idtext1.bottom
       anchors.right: parent.right
       anchors.rightMargin: parent.width * 0.2
       horizontalAlignment: Text.AlignRight
       verticalAlignment: Text.AlignVCenter
       fontSizeMode: Text.Fit
       font.pixelSize: height * 0.9
    }
}
