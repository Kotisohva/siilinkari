// Copyright (C) 2019 Pasi Kaukinen

// This file is part of Siilinkarin tuulet.

// Siilinkarin tuulet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Siilinkarin tuulet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Siilinkarin tuulet.  If not, see <https://www.gnu.org/licenses/>.

#ifndef POLLER_H
#define POLLER_H

#include <QObject>
#include <QDateTime>
#include <QNetworkAccessManager>
#include <QTimer>

enum measure_type
{
    notparsing,
    speed,
    gust,
    direction,
    temp
};


class Poller : public QObject
{
    Q_OBJECT
public:
    explicit Poller(QObject *parent = nullptr);
    ~Poller();

public slots:
    void poll(int iForecast = -1);
    void pollForecast();
    void onFinished(QNetworkReply*);
    void checkPollNeed(int iForecast);

    double maxTemp() {return mes_maxTemp; }
    double minTemp() {return mes_minTemp; }
    double maxWind() {return mes_maxWind; }
    double maxGust() {return mes_maxGust; }

    double maxTempForecast() {return fore_maxTemp; }
    double minTempForecast() {return fore_minTemp; }
    double maxWindForecast() {return fore_maxWind; }
    double maxGustForecast() {return fore_maxGust; }

    QDateTime maxTempTime() {return mes_maxTempTime; }
    QDateTime minTempTime() {return mes_minTempTime; }
    QDateTime maxWindTime() {return mes_maxWindTime; }
    QDateTime maxGustTime() {return mes_maxGustTime; }

    double measuredWind(QDateTime dt) { return timeline_wind[dt]; }
    double measuredGust(QDateTime dt) { return timeline_gust[dt]; }
    double measuredTemp(QDateTime dt) { return timeline_temp[dt]; }
    int measuredDir(QDateTime dt) { return timeline_direction[dt]; }

    double forecastedWind(QDateTime dt) { return forecast_wind[dt]; }
    double forecastedGust(QDateTime dt) { return forecast_gust[dt]; }
    double forecastedTemp(QDateTime dt) { return forecast_temp[dt]; }
    int forecastedDir(QDateTime dt) { return forecast_direction[dt]; }


    QList<QDateTime> measureTimes();
    QList<QDateTime> forecastTimes();

signals:
    void windChanged(double dSpeed, double dGust, int iDir, double dTemp, QDateTime tTime);
    void mesuresChanged();
    void networkError(int iCode, QString strError);

private slots:

    void onTimeout();
    void onHandleMeasures(QString str);
    void onHandleForecast(QString str);

private:

    void parseRawMeasureData(QString str);
    void parseRawForecastData(QString str);
    void reset(bool bSignalize = false);
    int nextTimeOut();
    QString errorString(QString strError);

    void dumbValues();

    QNetworkAccessManager* m_pManager;
    QNetworkRequest m_request;
    QTimer* m_pTimer;

    QHash<QDateTime, double> timeline_temp;
    QHash<QDateTime, double> timeline_wind;
    QHash<QDateTime, double> timeline_gust;
    QHash<QDateTime, int> timeline_direction;

    QHash<QDateTime, double> forecast_temp;
    QHash<QDateTime, double> forecast_wind;
    QHash<QDateTime, double> forecast_gust;
    QHash<QDateTime, int> forecast_direction;

    QDateTime last_time;
    QDateTime first_time;

    QDateTime forecast_last_time;
    QDateTime forecast_first_time;


    double      mes_maxTemp;
    QDateTime   mes_maxTempTime;
    double      mes_minTemp;
    QDateTime   mes_minTempTime;
    double      mes_maxWind;
    QDateTime   mes_maxWindTime;
    double      mes_maxGust;
    QDateTime   mes_maxGustTime;

    double      fore_maxGust;
    double      fore_maxWind;
    double      fore_maxTemp;
    double      fore_minTemp;

    bool m_bPolling;
    QString m_strPosition;
    int m_iForecast;


};

#endif // POLLER_H
