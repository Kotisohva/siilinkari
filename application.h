// Copyright (C) 2019 Pasi Kaukinen

// This file is part of Siilinkarin tuulet.

// Siilinkarin tuulet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Siilinkarin tuulet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Siilinkarin tuulet.  If not, see <https://www.gnu.org/licenses/>.

#ifndef APPLICATION_H
#define APPLICATION_H

#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQuickWindow>
#include <QDateTime>
#include <QTranslator>

class Poller;

class Application : public QApplication
{
    Q_OBJECT

    Q_PROPERTY(double speed READ windSpeed WRITE setWindSpeed NOTIFY windSpeedChanged)
    Q_PROPERTY(double gust READ windGust WRITE setWindGust NOTIFY windGustChanged)
    Q_PROPERTY(int dir READ windDir WRITE setWindDir NOTIFY windDirChanged)
    Q_PROPERTY(double temp READ temp WRITE setTemp NOTIFY tempChanged)
    Q_PROPERTY(QDateTime time READ windTime WRITE setWindTime NOTIFY windTimeChanged)

    Q_PROPERTY(double min_temp READ minTemp WRITE setMinTemp NOTIFY minTempChanged)
    Q_PROPERTY(QDateTime min_temp_time READ minTempTime WRITE setMinTempTime NOTIFY minTempTimeChanged)
    Q_PROPERTY(double max_temp READ maxTemp WRITE setMaxTemp NOTIFY maxTempChanged)
    Q_PROPERTY(QDateTime max_temp_time READ maxTempTime WRITE setMaxTempTime NOTIFY maxTempTimeChanged)

    Q_PROPERTY(double max_speed READ maxSpeed WRITE setMaxSpeed NOTIFY maxSpeedChanged)
    Q_PROPERTY(QDateTime max_speed_time READ maxSpeedTime WRITE setMaxSpeedTime NOTIFY maxSpeedTimeChanged)
    Q_PROPERTY(double max_gust READ maxGust WRITE setMaxGust NOTIFY maxGustChanged)
    Q_PROPERTY(QDateTime max_gust_time READ maxGustTime WRITE setMaxGustTime NOTIFY maxGustTimeChanged)

public:
    Application(int &argc, char **argv);
    ~Application();

    QString appFile();

    double windSpeed() { return m_WindSpeed; }
    double windGust() { return m_WindGust; }
    int windDir() { return m_WindDir; }
    double temp() { return m_Temp; }
    QDateTime windTime() { return m_WindTime; }

    double minTemp() { return m_minTemp; }
    QDateTime minTempTime() { return m_minTempTime; }
    double maxTemp() { return m_maxTemp; }
    QDateTime maxTempTime() { return m_maxTempTime; }

    double maxSpeed() { return m_maxSpeed; }
    QDateTime maxSpeedTime() { return m_maxSpeedTime; }
    double maxGust() { return m_maxGust; }
    QDateTime maxGustTime() { return m_maxGustTime; }

    void setWindSpeed(double d) { m_WindSpeed = d; emit windSpeedChanged(); }
    void setWindGust(double d) { m_WindGust = d; emit windGustChanged();}
    void setWindDir(int i) { m_WindDir = i; emit windDirChanged();}
    void setTemp(double d) { m_Temp = d; emit tempChanged();}
    void setWindTime(QDateTime t) { m_WindTime = t; emit windTimeChanged();}

    void setMaxSpeed(double d) {m_maxSpeed = d; emit maxSpeedChanged(); }
    void setMaxSpeedTime(QDateTime t) { m_maxSpeedTime = t; emit maxSpeedTimeChanged();}
    void setMaxGust(double d) {m_maxGust = d; emit maxGustChanged(); }
    void setMaxGustTime(QDateTime t) { m_maxGustTime = t; emit maxGustTimeChanged();}

    void setMinTemp(double d) {m_minTemp = d; emit minTempChanged(); }
    void setMinTempTime(QDateTime t) { m_minTempTime = t; emit minTempTimeChanged();}
    void setMaxTemp(double d) {m_maxTemp = d; emit maxTempChanged(); }
    void setMaxTempTime(QDateTime t) { m_maxTempTime = t; emit maxTempTimeChanged();}

    void readSettings();
    QVariant settingsValue(QString key, QVariant tVar=QVariant());

    QStringList settingsKeys();
    QStringList settingsGeneralKeys();
    QStringList settingsWindKeys();
    QStringList settingsTempKeys();
    QVariant defaultValue(QString strKey);

private Q_SLOTS:
    void onObjectCreated(QObject*, const QUrl&);
    void onWindChanged(double dSpeed, double dGust, int iDir, double dTemp, QDateTime tTime);
    void onMesuresChanged();

    void onSettingsValueChanged(QString strKey, QVariant dValue);
    void onPollRequested();
    void onApplicationStateChanged(Qt::ApplicationState tState);
    void onNetworkError(int iCode, QString strError);


Q_SIGNALS:
    void windSpeedChanged();
    void windGustChanged();
    void windDirChanged();
    void tempChanged();
    void windTimeChanged();

    void maxSpeedChanged();
    void maxSpeedTimeChanged();
    void maxGustChanged();
    void maxGustTimeChanged();

    void minTempChanged();
    void minTempTimeChanged();
    void maxTempChanged();
    void maxTempTimeChanged();

private:

    QString settingsFile();
    QString defaultLanguage();
    void setLanguage(QString strLanguage);

    QQmlApplicationEngine* m_pEngine;
    Poller* m_pPoller;
    QObject* m_pRootObject;
    bool m_bRunning;

    QTranslator* m_pTranslator;

    double m_WindSpeed;
    double m_WindGust;
    int m_WindDir;
    double m_Temp;
    QDateTime m_WindTime;

    double m_minTemp;
    QDateTime m_minTempTime;
    double m_maxTemp;
    QDateTime m_maxTempTime;
    double m_maxSpeed;
    QDateTime m_maxSpeedTime;
    double m_maxGust;
    QDateTime m_maxGustTime;

};

#endif // APPLICATION_H
