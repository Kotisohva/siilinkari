// Copyright (C) 2019 Pasi Kaukinen

// This file is part of Siilinkarin tuulet.

// Siilinkarin tuulet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Siilinkarin tuulet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Siilinkarin tuulet.  If not, see <https://www.gnu.org/licenses/>.

#include "application.h"

int main(int argc, char *argv[])
{
    Application app(argc, argv);

    app.setOrganizationName("Kotisohvan sovellukset");
    app.setOrganizationDomain("kotisohva.net");
    app.setApplicationName("Siilinkarin tuulet");
    app.setApplicationVersion("1.8.5");

    return app.exec();
}
