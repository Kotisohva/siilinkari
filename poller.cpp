// Copyright (C) 2019 Pasi Kaukinen

// This file is part of Siilinkarin tuulet.

// Siilinkarin tuulet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Siilinkarin tuulet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Siilinkarin tuulet.  If not, see <https://www.gnu.org/licenses/>.


#include <QDebug>
#include <QNetworkReply>
#include <application.h>
#include "poller.h"

static const double NOTSET = -256;

static const QString FMI_STATION  = QString("http://opendata.fmi.fi/wfs?request=getFeature&storedquery_id=fmi::observations::weather::timevaluepair&timestep=10&fmisid=%1");
static const QString FMI_FORECAST = QString("http://opendata.fmi.fi/wfs?request=getFeature&storedquery_id=fmi::forecast::harmonie::surface::point::timevaluepair&timestep=30&parameters=WindSpeedMS,WindGust,WindDirection,Temperature&latlon=%1");


Poller::Poller(QObject *parent) : QObject(parent),
    m_pManager(nullptr), m_pTimer(nullptr), m_bPolling(false), m_iForecast(0)
{
    m_pManager = new QNetworkAccessManager();
    connect(m_pManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(onFinished(QNetworkReply*)));

    m_pTimer = new QTimer(this);
    connect(m_pTimer, SIGNAL(timeout()), this, SLOT(onTimeout()));
    m_pTimer->start(nextTimeOut());

    reset();
}

int Poller::nextTimeOut()
{
    QDateTime t = QDateTime::currentDateTime();
    qDebug() << "Now:  " << t.time();

    int iMSec = t.time().msecsSinceStartOfDay();
    int iTenMins = iMSec/600000;
    int iNextMSec = (iTenMins+1) * 600000;
    int iSep = iNextMSec - iMSec + 60000;

    t = t.addMSecs(iSep);
    qDebug() << "Next: "<< t.time();

    return iSep;
}

Poller::~Poller()
{
    m_pManager->deleteLater();
    m_pManager = nullptr;
    m_pTimer->stop();
    m_pTimer->deleteLater();
    m_pTimer = nullptr;
}

void Poller::checkPollNeed(int iForecast)
{
    if(!m_pManager || m_bPolling)
        return;

    QDateTime t = QDateTime::currentDateTime();
    bool b = false;
    if(!last_time.isValid())
        b = true;
    else if(last_time.secsTo(t)>=660)
        b = true;

    if(b)
    {
        qDebug() << "Check: Poll needed";
        poll(iForecast);
    }
    else
        qDebug() << "Check: No poll needed";
}

void Poller::onTimeout()
{
    QDateTime t = QDateTime::currentDateTime();
    qDebug() << "Poll: " << t.time();
    m_pTimer->stop();
    poll();
}


void Poller::poll(int iForecast)
{
    if(!m_pManager || m_bPolling)
        return;

    qDebug() << "Polling requested";
    m_bPolling = true;
    reset(true);

    if(iForecast>=0)
        m_iForecast = iForecast;

    int iStationId = dynamic_cast<Application*>(qApp)->settingsValue("station_id", 101311).toInt();
    QString strStationName = dynamic_cast<Application*>(qApp)->settingsValue("station_name", "Siilinkari").toString();
    qDebug() << "Getting data from station id" << iStationId << qPrintable(strStationName);
    m_request.setUrl(QUrl(FMI_STATION.arg(iStationId)));
    m_pManager->get(m_request);
}

void Poller::pollForecast()
{
    if(!m_pManager || m_bPolling)
        return;

    qDebug() << "Polling forecast requested";
    m_bPolling = true;

    QString strStationName = dynamic_cast<Application*>(qApp)->settingsValue("station_name", "Siilinkari").toString();
    qDebug() << "Getting forecast data for station " << qPrintable(strStationName) << " " << qPrintable(m_strPosition);
    m_request.setUrl(QUrl(FMI_FORECAST.arg(m_strPosition)));
    m_pManager->get(m_request);
}


void Poller::onFinished(QNetworkReply* pReply)
{
    int iCode = int(pReply->error());
    QString strError = errorString(pReply->errorString());
    if(iCode)
    {
        int iCode = int(pReply->error());
        qDebug() << "Query finished on network error: " << iCode << qPrintable(strError);
        pReply->deleteLater();
        m_bPolling = false;
        m_pTimer->start(10000);
        emit networkError(iCode, strError);
        return;
    }
    qDebug() << "Response" << int(pReply->size()/1000) << " kb" ;
    QString str = pReply->readAll();

    if(pReply->url().toString().contains("observations"))
    {
        onHandleMeasures(str);
        pollForecast();

    }
    else if(pReply->url().toString().contains("forecast"))
    {
        onHandleForecast(str);
    }

    pReply->deleteLater();

}

void Poller::onHandleForecast(QString str)
{
    parseRawForecastData(str);
    m_bPolling = false;

    emit mesuresChanged();

//    QList<QDateTime> keys = forecastTimes();
//    foreach(QDateTime key, keys)
//    {
//        qDebug() << qPrintable(key.time().toString("hh:mm")) << forecast_wind[key] << forecast_gust[key] << forecast_direction[key] << forecast_temp[key];
//    }
}

void Poller::onHandleMeasures(QString str)
{
    parseRawMeasureData(str);
    m_bPolling = false;

    dumbValues();

    QList<QDateTime> tMeasureTimes = measureTimes();

    double dLastWind = timeline_wind.value(last_time, -1);
    double dLastGust = timeline_gust.value(last_time, -1);
    int iLastDir = timeline_direction.value(last_time, -1);
    double dLastTemp = timeline_temp.value(last_time, NOTSET);

    while( (dLastWind<0 || dLastGust<0 || iLastDir<0 || dLastTemp==NOTSET) && timeline_wind.count()>0)
    {
        // Failure on last data, delivered from FMI
        qDebug() << "Removing failed data at " << qPrintable(last_time.time().toString());
        timeline_wind.remove(last_time);
        timeline_gust.remove(last_time);
        timeline_direction.remove(last_time);
        timeline_temp.remove(last_time);
        last_time = measureTimes().last();
        dLastWind = timeline_wind.value(last_time, -1);
        dLastGust = timeline_gust.value(last_time, -1);
        iLastDir = timeline_direction.value(last_time, -1);
        dLastTemp = timeline_temp.value(last_time, -NOTSET);
        dumbValues();
        emit networkError(0, tr("Tuorein mittausdata ei saatavilla"));
    }

    emit windChanged(dLastWind, dLastGust, iLastDir, dLastTemp, last_time);
    m_pTimer->start(nextTimeOut());
}

void Poller::reset(bool bSignalize)
{
    foreach(QDateTime key, measureTimes())
    {
        timeline_wind[key]=-1;
        timeline_gust[key]=-1;
        timeline_direction[key]=-1;
        timeline_temp[key]=NOTSET;
    }

    foreach(QDateTime key, forecastTimes())
    {
        forecast_wind[key]=-1;
        forecast_gust[key]=-1;
        forecast_direction[key]=-1;
        forecast_temp[key]=NOTSET;
    }


    mes_maxTemp = NOTSET;
    mes_minTemp = NOTSET;
    mes_maxWind = -1;
    mes_maxGust = -1;

    fore_maxTemp = NOTSET;
    fore_minTemp = NOTSET;
    fore_maxGust = -1;
    fore_maxWind = -1;


    // Clear chart, but use still old x/y labels
    if(bSignalize)
    {
        emit windChanged(-1, -1, -1, NOTSET, QDateTime() );
        emit mesuresChanged();
    }

    timeline_temp.clear();
    timeline_direction.clear();
    timeline_wind.clear();
    timeline_gust.clear();

    forecast_temp.clear();
    forecast_direction.clear();
    forecast_wind.clear();
    forecast_gust.clear();

    m_strPosition.clear();


}

void Poller::parseRawMeasureData(QString str)
{
    double measure_value = NOTSET;
    QDateTime measure_time = QDateTime();
    measure_type parsing_state = measure_type::notparsing;
    bool valid_time = false;
    first_time = QDateTime();
    last_time = QDateTime();

    QStringList lines = str.split('\n');
    foreach (QString s, lines)
    {
        switch (parsing_state)
        {
        case measure_type::notparsing:
        {
            // Stastion position?
            if(m_strPosition.isEmpty() && s.contains("gml:pos"))
            {
                m_strPosition = s.replace("<gml:pos>", "").replace("</gml:pos>", "").trimmed().replace(" ", ",");
                qDebug() << "Station position: " << qPrintable(m_strPosition);
            }
            // Are we onto measure series?
            if(!s.contains("<wml2:MeasurementTimeseries"))
                break;

            // And what data is coming next?
            if (s.contains("ws_10min"))
                parsing_state = measure_type::speed;
            else if (s.contains("wg_10min"))
                parsing_state = measure_type::gust;
            else if (s.contains("wd_10min"))
                parsing_state = measure_type::direction;
            else if (s.contains("t2m"))
                parsing_state = measure_type::temp;
            break;
        }
        case measure_type::temp:
        case measure_type::speed:
        case measure_type::gust:
        case measure_type::direction:
        {
            // Measure serie ends?
            if(s.contains("</wml2:MeasurementTimeseries"))
            {
                parsing_state = measure_type::notparsing;
                valid_time = false;
                break;
            }

            // Do we have time value?
            if(s.contains("wml2:time"))
            {
                QString time = s.replace("<wml2:time>", "").replace("</wml2:time>", "").trimmed();
                time = time.replace("T", " ").replace("Z", "");
                QDateTime utc_time = QDateTime::fromString(time, "yyyy-MM-dd HH:mm:ss");
                utc_time.setTimeSpec(Qt::UTC);
                measure_time = utc_time.toLocalTime();
                valid_time = measure_time.isValid();
                measure_value = NOTSET;
            }
            else if (s.contains("wml2:value"))
            {
                QString value = s.replace("<wml2:value>", "").replace("</wml2:value>", "").trimmed();
                if (value == "NaN" || !valid_time)
                    break;

                bool b = false;
                b = measure_value = value.toDouble(&b);
                if(!b)
                    break;


                if (parsing_state == measure_type::temp)
                {
                    if(measure_value > 100 || measure_value < -100)
                    {
                        valid_time = false;
                        break;
                    }
                    timeline_temp[measure_time] = measure_value;
                    if (measure_value > mes_maxTemp)
                    {
                        mes_maxTemp = measure_value;
                        mes_maxTempTime = measure_time;
                    }
                    if (measure_value < mes_minTemp || int(mes_minTemp) == NOTSET)
                    {
                        mes_minTemp = measure_value;
                        mes_minTempTime = measure_time;
                    }
                }
                else if (parsing_state == measure_type::speed)
                {
                    if(measure_value > 300 || measure_value < 0)
                    {
                        valid_time = false;
                        break;
                    }
                    timeline_wind[measure_time] = measure_value;
                    if (measure_value > mes_maxWind)
                    {
                        mes_maxWind = measure_value;
                        mes_maxWindTime = measure_time;
                    }
                }
                else if (parsing_state == measure_type::gust)
                {
                    if(measure_value > 300 || measure_value < 0)
                    {
                        valid_time = false;
                        break;
                    }
                    timeline_gust[measure_time] = measure_value;
                    if (measure_value > mes_maxGust)
                    {
                        mes_maxGust = measure_value;
                        mes_maxGustTime = measure_time;
                    }
                }
                else if (parsing_state == measure_type::direction)
                {
                    if(measure_value > 360 || measure_value < 0)
                    {
                        valid_time = false;
                        break;
                    }
                    timeline_direction[measure_time] = measure_value;
                }

                if (valid_time && (!first_time.isValid() || measure_time < first_time) && measure_value != NOTSET)
                    first_time = measure_time;
                if (valid_time && (!last_time.isValid() ||measure_time > last_time) && measure_value != NOTSET)
                    last_time  = measure_time;

                valid_time = false;
                break;
            }

            break;
        } // case

        } //switch

    } //foreach

    return;

}

void Poller::parseRawForecastData(QString str)
{
    double measure_value = NOTSET;
    QDateTime measure_time = QDateTime();
    measure_type parsing_state = measure_type::notparsing;
    bool valid_time = false;
    forecast_first_time = QDateTime();
    forecast_last_time = QDateTime();
    QDateTime tForecastShouldEnd = QDateTime();

    QStringList lines = str.split('\n');
    foreach (QString s, lines)
    {
        switch (parsing_state)
        {
        case measure_type::notparsing:
        {
            // Are we onto measure series?
            if(!s.contains("<wml2:MeasurementTimeseries"))
                break;

            // And what data is coming next?
            if (s.contains("WindSpeedMS"))
                parsing_state = measure_type::speed;
            else if (s.contains("WindGust"))
                parsing_state = measure_type::gust;
            else if (s.contains("WindDirection"))
                parsing_state = measure_type::direction;
            else if (s.contains("Temperature"))
                parsing_state = measure_type::temp;
            break;
        }
        case measure_type::temp:
        case measure_type::speed:
        case measure_type::gust:
        case measure_type::direction:
        {
            // Measure serie ends?
            if(s.contains("</wml2:MeasurementTimeseries"))
            {
                parsing_state = measure_type::notparsing;
                valid_time = false;
                break;
            }

            // Do we have time value?
            if(s.contains("wml2:time"))
            {
                QString time = s.replace("<wml2:time>", "").replace("</wml2:time>", "").trimmed();
                time = time.replace("T", " ").replace("Z", "");
                QDateTime utc_time = QDateTime::fromString(time, "yyyy-MM-dd HH:mm:ss");
                utc_time.setTimeSpec(Qt::UTC);
                measure_time = utc_time.toLocalTime();
                valid_time = measure_time.isValid();
                measure_value = NOTSET;
            }
            else if (s.contains("wml2:value"))
            {
                QString value = s.replace("<wml2:value>", "").replace("</wml2:value>", "").trimmed();
                if (value == "NaN" || !valid_time)
                {
                    parsing_state = measure_type::notparsing;
                    valid_time = false;
                    break;
                }

                if( (valid_time && tForecastShouldEnd.isValid() &&  measure_time > tForecastShouldEnd) || measure_time <= last_time)
                {
                    parsing_state = measure_type::notparsing;
                    valid_time = false;
                    break; // discard unneeded values
                }

                bool b = false;
                b = measure_value = value.toDouble(&b);
                if(!b)
                    break;

                if (parsing_state == measure_type::temp)
                {
                    if(measure_value > 100 || measure_value < -100)
                    {
                        valid_time = false;
                        break;
                    }
                    forecast_temp[measure_time] = measure_value;
                    if (measure_value > fore_maxTemp)
                    {
                        fore_maxTemp = measure_value;
                    }
                    if (measure_value < fore_minTemp || int(fore_minTemp) == NOTSET)
                    {
                        fore_minTemp = measure_value;
                    }
                }
                else if (parsing_state == measure_type::speed)
                {
                    if(measure_value > 300 || measure_value < 0)
                    {
                        valid_time = false;
                        break;
                    }
                    forecast_wind[measure_time] = measure_value;
                    if (measure_value > fore_maxWind)
                    {
                        fore_maxWind = measure_value;
                    }

                }
                else if (parsing_state == measure_type::gust)
                {
                    if(measure_value > 300 || measure_value < 0)
                    {
                        valid_time = false;
                        break;
                    }
                    forecast_gust[measure_time] = measure_value;
                    if (measure_value > fore_maxGust)
                    {
                        fore_maxGust = measure_value;
                    }

                }
                else if (parsing_state == measure_type::direction)
                {
                    if(measure_value > 360 || measure_value < 0)
                    {
                        valid_time = false;
                        break;
                    }
                    forecast_direction[measure_time] = measure_value;
                }

                if (valid_time && (!forecast_first_time.isValid() || measure_time < forecast_first_time) && measure_value != NOTSET)
                {
                    forecast_first_time = measure_time;
                    if(!tForecastShouldEnd.isValid())
                        tForecastShouldEnd = measure_time.addSecs(m_iForecast*3600);
                }
                if (valid_time && (!forecast_last_time.isValid() ||measure_time > forecast_last_time) && measure_value != NOTSET)
                {
                    forecast_last_time  = measure_time;
                }
                valid_time = false;
                break;
            }

            break;
        } // case

        } //switch

    } //foreach

    return;

}


bool dtcomp(QDateTime left, QDateTime right) {
  return left < right;
}

QList<QDateTime> Poller::measureTimes()
{
    QList<QDateTime> tMeasureTimes = timeline_wind.keys();
    std::sort(tMeasureTimes.begin(), tMeasureTimes.end(), dtcomp);
    return tMeasureTimes;
}

QList<QDateTime> Poller::forecastTimes()
{
    QList<QDateTime> tForecastTimes = forecast_wind.keys();
    std::sort(tForecastTimes.begin(), tForecastTimes.end(), dtcomp);
    return tForecastTimes;
}

QString Poller::errorString(QString strError)
{
    QString strRet = strError;
    if(strError=="Host opendata.fmi.fi not found")
        strRet = tr("Host opendata.fmi.fi not found");
    else if(strError=="Network access is disabled.")
        strRet = tr("Network access is disabled.");

    return strRet;
}

void Poller::dumbValues()
{
    QList<QDateTime> tMeasureTimes = measureTimes();
    foreach(QDateTime tMeasure, tMeasureTimes)
    {
        qDebug() << qPrintable(tMeasure.time().toString())<<" "<<timeline_wind[tMeasure]<<" "<<timeline_gust[tMeasure]<<timeline_direction[tMeasure] <<"  "<< timeline_temp[tMeasure];
    }
}
