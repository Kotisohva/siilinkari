// Copyright (C) 2019 Pasi Kaukinen

// This file is part of Siilinkarin tuulet.

// Siilinkarin tuulet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Siilinkarin tuulet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Siilinkarin tuulet.  If not, see <https://www.gnu.org/licenses/>.

import QtQuick
import QtQuick.Controls
import QtQuick.Controls.Material

Item {
    id: textpair
    property var c_color: Material.foreground

    property alias text1: idtext1.text
    property alias text2: idtext2.text
    property alias text3: idtext3.text

    property alias rotation1: idtext1.rotation
    property alias horizontalAlignment1: idtext1.horizontalAlignment
    property alias verticalAlignment1: idtext1.verticalAlignment

    property double d_width_factor: 0.65

    width: isPortrait() ? page_root.width : page_root.width * 0.5
    height: isPortrait() ? page_root.height * 0.2 : page_root.height * 0.4

    Label {
       id: idtext1
       text: ""
       color: parent.c_color
       font.bold: true
       width: parent.width * d_width_factor
       height: parent.height
       anchors.bottom: parent.bottom
       anchors.left: parent.left
       anchors.leftMargin: rotation === 0 ? 0 : isPortrait() ? parent.width * 0.1 : 0
       horizontalAlignment: Text.AlignRight
       verticalAlignment: Text.AlignVCenter
       fontSizeMode: Text.Fit
       font.pixelSize: rotation === 0 ? height*0.6 : height*0.6
       Behavior on text {
           FadeAnimation { target: idtext1 }
       }
    }
    Text {
       id: idtext2
       text: ""
       color: parent.c_color
       width: parent.width * (0.97 - d_width_factor)
       height: parent.height * 0.45
       anchors.bottom: idtext3.top
       anchors.right: parent.right
       anchors.rightMargin: rotation === 0 ? 0 : isPortrait() ? 0 : parent.width * 0.2
       horizontalAlignment: Text.AlignLeft
       verticalAlignment: Text.AlignBottom
       fontSizeMode: Text.Fit
       font.pixelSize: height*0.6
       Behavior on text {
           FadeAnimation { target: idtext2 }
       }
    }
    Text {
       id: idtext3
       text: ""
       color: parent.c_color
       width: parent.width * (0.97 - d_width_factor)
       height: parent.height * 0.45
       anchors.bottom: parent.bottom
       anchors.right: parent.right
       horizontalAlignment: Text.AlignLeft
       verticalAlignment: Text.AlignTop
       fontSizeMode: Text.Fit
       font.pixelSize: height*0.3
       Behavior on text {
           FadeAnimation { target: idtext3 }
       }
    }
}
