// Copyright (C) 2019 Pasi Kaukinen

// This file is part of Siilinkarin tuulet.

// Siilinkarin tuulet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Siilinkarin tuulet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Siilinkarin tuulet.  If not, see <https://www.gnu.org/licenses/>.

import QtQuick
import QtQuick.Controls
import QtQuick.Controls.Material

Page {
    id: page_root
    width: root.width
    height: root.height
    font.capitalization: Font.AllUppercase
    header: Label {
        id: label_header
        text: root.station_name
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    FlickablePage {

        Text {
            id: wind_time
            text: toCustom(app.time)
            color: "#808080"
            width: page_root.width
            height: page_root.height * 0.06
            leftPadding: 10
            fontSizeMode: Text.Fit
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignTop
            font.pixelSize: Qt.application.font.pixelSize * 2
            function toCustom(t)
            {
                return t.toLocaleString('fi-FI')
            }
        }
        TextPair {
            id: wind_gust
            text1: app.gust<0 ? "" : app.gust.toFixed(1)
            text2: app.gust<0 ? "" : qsTr("m/s")
            text3: app.gust<0 ? "" : qsTr("puuskatuuli")
            c_color: getWindColor(app.gust)
            anchors.left: parent.left
            anchors.top: wind_time.bottom
            anchors.topMargin:  0 //page_root.height * 0.1
        }
        TextPair {
            id: wind_speed
            text1: app.speed<0 ? "" : app.speed.toFixed(1)
            text2: app.speed<0 ? "" : qsTr("m/s")
            text3: app.gust<0 ? "" : qsTr("keskituuli")
            c_color: getWindColor(app.speed)
            anchors.left: parent.left
            anchors.top: wind_gust.bottom
        }
        TextPair {
            id: wind_dir
            width: isPortrait() ? page_root.width * 0.8 : page_root.width * 0.4
            text1: app.dir<0 ? "" : "↓"
            text2: app.dir<0 ? "" : app.dir + "°"
            text3: getWindDirection(app.dir)
            rotation1: app.dir
            horizontalAlignment1: Text.AlignHCenter
            verticalAlignment1: Text.AlignVCenter
            anchors.left: isPortrait() ? parent.left : wind_gust.right
            anchors.leftMargin:  isPortrait() ? width * 0.17 : width * 0.12
            anchors.top: isPortrait() ? wind_speed.bottom : wind_time.bottom
        }
        TextSingle {
            id: temp
            text1: app.temp<-255 ? "" : "<b>"+app.temp.toFixed(1)+ "</b> °C"
            color1: getTempColor(app.temp)
            anchors.left: isPortrait() ? parent.left : wind_speed.right
            anchors.leftMargin: isPortrait() ? width * 0.05 : width * 0.1
            anchors.top: wind_dir.bottom
        }
        Image {
            anchors.fill: parent
            id: backgroundImage
            opacity: dark() ? 0.25 : 0.7
            fillMode: Image.PreserveAspectFit
            source: root.backGroundImage()
            z: -1
        }
    }
}
