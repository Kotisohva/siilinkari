// Copyright (C) 2019 Pasi Kaukinen

// This file is part of Siilinkarin tuulet.

// Siilinkarin tuulet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Siilinkarin tuulet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Siilinkarin tuulet.  If not, see <https://www.gnu.org/licenses/>.

import QtQuick
import QtQuick.Controls

Page {
    id: settings_page
    width: root.width
    height: root.height
    header: Label {
        id: label_header
        text: qsTr("ASETUKSET   1/3")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    onVisibleChanged:
    {
        if(visible===true)
        {
            sortLanguages()
            forecast_combobox.findIndex()
        }
    }

    function sortLanguages()
    {
        console.log("Sorting languages")
        station_combobox.model.sortModel()
    }

    // Station
    Label {
        id: text_station
        text: qsTr("Mittausasema")
        font.pixelSize: height * 0.7
        padding: 10
        width: isPortrait() ? parent.width : parent.width * 0.5
        height: isPortrait() ? root.height * 0.05 : root.height * 0.1
        anchors.top: parent.top
        anchors.topMargin: isPortrait() ? 20 : 5
        anchors.left: parent.left
        verticalAlignment: Text.AlignVCenter
    }
    ComboBox {
        id: station_combobox
        width: isPortrait() ? parent.width * 0.8 : parent.width * 0.4
        anchors.top: text_station.bottom
        textRole: "name"
        height: isPortrait() ? root.height * 0.06 : root.height * 0.12
        font.pixelSize: height*0.4
        anchors.left: parent.left
        anchors.leftMargin: root.height * 0.05
        delegate: ItemDelegate {
            width: station_combobox.width
            height: station_combobox.height
            text: station_combobox.textRole ? (Array.isArray(station_combobox.model) ? modelData[station_combobox.textRole] : model[station_combobox.textRole]) : modelData
            font.weight: Font.Normal
            font.family: station_combobox.font.family
            font.pointSize: station_combobox.font.pointSize
            highlighted: station_combobox.highlightedIndex === index
            hoverEnabled: station_combobox.hoverEnabled
        }
        model: ListModel {
            id: station_model
            ListElement { name: qsTr("Asikkala Pulkkilanharju"); value: 101185 }
            ListElement { name: qsTr("Hailuoto Marjaniemi"); value: 101784 }
            ListElement { name: qsTr("Hanko Russarö"); value: 100932 }
            ListElement { name: qsTr("Hanko Tulliniemi"); value: 100946 }
            ListElement { name: qsTr("Helsinki Harmaja"); value: 100996 }
            ListElement { name: qsTr("Hammarland Märket"); value: 100919 }
            ListElement { name: qsTr("Helsinki Majakka"); value: 101003 }
            ListElement { name: qsTr("Inari Seitalaassa"); value: 129963 }
            ListElement { name: qsTr("Inkoo Bågaskär"); value: 100969 }
            ListElement { name: qsTr("Kalajoki Ulkokalla"); value: 101673 }
            ListElement { name: qsTr("Kaskinen Sälgrund"); value: 101256 }
            ListElement { name: qsTr("Kemi Ajos"); value: 101846 }
            ListElement { name: qsTr("Kemi I majakka"); value: 101783 }
            ListElement { name: qsTr("Kemiönsaari Vänö"); value: 100945 }
            ListElement { name: qsTr("Kirkkonummi Mäkiluoto"); value: 100997 }
            ListElement { name: qsTr("Kokkola Tankar"); value: 101661 }
            ListElement { name: qsTr("Kotka Haapasaari"); value: 101042 }
            ListElement { name: qsTr("Kotka Rankki"); value: 101030 }
            ListElement { name: qsTr("Kuopio Ritoniemi"); value: 101580 }
            ListElement { name: qsTr("Kustavi Isokari"); value: 101059 }
            ListElement { name: qsTr("Korsnäs Bredskäret"); value: 101479 }
            ListElement { name: qsTr("Kristiinankaupunki majakka"); value: 101268 }
            ListElement { name: qsTr("Kumlinge kirkonkylä"); value: 100928 }
            ListElement { name: qsTr("Kökar Bogskär"); value: 100921 }
            ListElement { name: qsTr("Lappeenranta Hiekkapakka"); value: 101252 }
            ListElement { name: qsTr("Lemland Nyhamn"); value: 100909 }
            ListElement { name: qsTr("Liperi Tuiskavanluoto"); value: 101628 }
            ListElement { name: qsTr("Lumparland Långnäs"); value: 151048 }
            ListElement { name: qsTr("Luhanka Judinsalo"); value: 101362 }
            ListElement { name: qsTr("Loviisa Orrengrund"); value: 101039 }
            ListElement { name: qsTr("Maalahti Strömmingsbådan"); value: 101481 }
            ListElement { name: qsTr("Mustasaari Valassaaret"); value: 101464 }
            ListElement { name: qsTr("Oulu Vihreäsaari"); value: 101794 }
            ListElement { name: qsTr("Parainen Fagerholm"); value: 100924 }
            ListElement { name: qsTr("Parainen Utö"); value: 100908 }
            ListElement { name: qsTr("Pietarsaari Kallan"); value: 101660 }
            ListElement { name: qsTr("Pori Tahkoluoto"); value: 101267 }
            ListElement { name: qsTr("Porvoo Emäsalo"); value: 101023 }
            ListElement { name: qsTr("Porvoo Kalbådagrund"); value: 101022 }
            ListElement { name: qsTr("Raasepori Jussarö"); value: 100965 }
            ListElement { name: qsTr("Rantasalmi Rukkasluoto"); value: 101436 }
            ListElement { name: qsTr("Raahe Nahkiainen"); value: 101775 }
            ListElement { name: qsTr("Raahe Lapaluoto"); value: 101785 }
            ListElement { name: qsTr("Rauma Kylmäpihlaja"); value: 101061 }
            ListElement { name: qsTr("Sipoo Itätoukki"); value: 105392 }
            ListElement { name: qsTr("Tampere Siilinkari"); value: 101311 }
            ListElement { name: qsTr("Turku Rajakari"); value: 100947 }

            function sortModel()
            {
                for(var i=0; i<count-1; i++)
                {
                    for(var j=i+1; j<count; j++)
                    {
                        if(qsTranslate("SettingsPage1",get(i).name) > qsTranslate("SettingsPage1",get(j).name))
                        {
                            move(j,i,1)
                        }
                    }
                }
                // Select current index
                station_combobox.currentIndex = -1
                for(var i=0; i<count; i++)
                {
                    if(get(i).value===root.station_id)
                    {
                        station_combobox.currentIndex = i;
                        break
                    }
                }

            }

        }
        onCurrentIndexChanged: {
            var shorty = shortName(station_model.get(currentIndex).name)
            root.settingsValueChanged("station_id", station_model.get(currentIndex).value)
            root.station_id = station_model.get(currentIndex).value
            root.settingsValueChanged("station_name", shorty)
            root.station_name = shorty
            root.refresh()
        }

        function shortName(longname)
        {
            var list = longname.split(" ")
            list.shift();
            return list.join(" ")
        }

    }

    // Forecast
    Label {
        id: text_forecast
        text: qsTr("Ennuste")
        font.pixelSize: height * 0.7
        padding: 10
        width: isPortrait() ? parent.width : parent.width * 0.5
        height: isPortrait() ? root.height * 0.05 : root.height * 0.1
        anchors.top: station_combobox.bottom
        anchors.topMargin: isPortrait() ? 20 : 5
        anchors.left: parent.left
        verticalAlignment: Text.AlignVCenter
    }
    ComboBox {
        id: forecast_combobox
        width: isPortrait() ? parent.width * 0.8 : parent.width * 0.4
        anchors.top: text_forecast.bottom
        textRole: "name"
        height: isPortrait() ? root.height * 0.06 : root.height * 0.12
        font.pixelSize: height*0.4
        anchors.left: parent.left
        anchors.leftMargin: root.height * 0.05
        delegate: ItemDelegate {
            width: forecast_combobox.width
            height: forecast_combobox.height
            text: forecast_combobox.textRole ? (Array.isArray(forecast_combobox.model) ? modelData[forecast_combobox.textRole] : model[forecast_combobox.textRole]) : modelData
            font.weight: Font.Normal
            font.family: forecast_combobox.font.family
            font.pointSize: forecast_combobox.font.pointSize
            highlighted: forecast_combobox.highlightedIndex === index
            hoverEnabled: forecast_combobox.hoverEnabled
        }
        model: ListModel {
            id: forecast_model
            ListElement { name: qsTr("Ei ennustetta"); value: 0 }
            ListElement { name: qsTr("3 seuraavaa tuntia"); value: 3 }
            ListElement { name: qsTr("6 seuraavaa tuntia"); value: 6 }
            ListElement { name: qsTr("12 seuraavaa tuntia"); value: 12 }
        }
        onCurrentIndexChanged: {
            var forecast = forecast_model.get(currentIndex).value
            root.settingsValueChanged("forecast", forecast)
            root.refresh()
        }
        function findIndex()
        {
            // Select current index
            forecast_combobox.currentIndex = -1
            for(var i=0; i<forecast_model.count; i++)
            {
                if(forecast_model.get(i).value===root.forecast)
                {
                    forecast_combobox.currentIndex = i;
                    break
                }
            }
        }
    }

    // Theme
    Label {
        id: text_theme
        text: qsTr("Teema")
        font.pixelSize: height * 0.7
        padding: 10
        width: isPortrait() ? parent.width : parent.width * 0.5
        height: isPortrait() ? root.height * 0.05 : root.height * 0.1
        anchors.top: isPortrait() ? forecast_combobox.bottom : parent.top
        anchors.topMargin: 20
        anchors.right: parent.right
        verticalAlignment: Text.AlignVCenter
    }
    ButtonGroup {id: themeGroup}
    SettingsThemeButton {
        id: radio_light
        anchors.top: text_theme.bottom
        svalue: 1
        text: qsTr("Vaalea")
    }
    SettingsThemeButton {
        id: radio_dark
        anchors.top: radio_light.bottom
        svalue: 0
        text: qsTr("Tumma")
    }

    // Language
    Label {
        id: text_language
        text: qsTr("Kieli")
        font.pixelSize: height * 0.7
        padding: 10
        width: isPortrait() ? parent.width : parent.width * 0.5
        height: isPortrait() ? root.height * 0.05 : root.height * 0.1
        anchors.top: radio_dark.bottom
        anchors.topMargin: isPortrait() ? 20 : 5
        anchors.right: parent.right
        verticalAlignment: Text.AlignVCenter
    }
    ButtonGroup {id: languageGroup}
    SettingsLanguageButton {
        id: radio_finnish
        anchors.top: text_language.bottom
        svalue: "fi"
        text: qsTr("Suomi")
    }
    SettingsLanguageButton {
        id: radio_swedish
        anchors.top: radio_finnish.bottom
        svalue: "se"
        text: qsTr("Svenska")
    }
    SettingsLanguageButton {
        id: radio_english
        anchors.top: radio_swedish.bottom
        svalue: "en"
        text: qsTr("English")
    }
}


