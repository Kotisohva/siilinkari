﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Threading;  
using Microsoft.Phone.Controls;

// Telerik
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Primitives;
using Telerik.Windows.Controls.Chart;

// Defines by ourself
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Collections;

namespace siilinkari
{
    public class WindDataCollection : List<WindData>
    {
        public WindDataCollection() { }
    }

    public class WindData
    {
        public DateTime Time {get; set; }
        public double Gust { get; set; }
        public double Speed { get; set; }
    }

    public class TempDataCollection : List<TempData>
    {
        public TempDataCollection() { }
    }

    public class TempData
    {
        public DateTime Time { get; set; }
        public double Temp { get; set; }
    }

    public partial class MainPage : PhoneApplicationPage
    {

        private Dictionary<DateTime, double> timeline_temp = new Dictionary<DateTime, double>(73);
        private Dictionary<DateTime, double> timeline_speed = new Dictionary<DateTime, double>(73);
        private Dictionary<DateTime, double> timeline_gust = new Dictionary<DateTime, double>(73);
        private Dictionary<DateTime, double> timeline_direction = new Dictionary<DateTime, double>(73);

        private WindDataCollection windDataCollection = new WindDataCollection();
        private TempDataCollection tempDataCollection = new TempDataCollection();

        private DateTime last_time = DateTime.MinValue;
        private DateTime first_time = DateTime.MaxValue;

        private Double      mes_maxGust = Double.MinValue;
        private DateTime    mes_maxGustTime = DateTime.MinValue;
        private Double      mes_maxSpeed = Double.MinValue;
        private DateTime    mes_maxSpeedTime = DateTime.MinValue;
        private Double      mes_maxTemp = Double.MinValue;
        private DateTime    mes_maxTempTime = DateTime.MinValue;
        private Double      mes_minTemp = Double.MaxValue;
        private DateTime    mes_minTempTime = DateTime.MinValue;


        private WebClient webClient;
        private const string downloadUrl = "http://data.fmi.fi/fmi-apikey/ebcb18fc-9a4e-450b-8795-26eeeb34e590/wfs?request=getFeature&storedquery_id=fmi::observations::weather::timevaluepair&fmisid=101311";

        enum measure_type
        {
            notparsing,
            speed,
            gust,
            direction,
            temp
        }

        // Constructor
        public MainPage()
        {
            InitializeComponent();
            clearScreenValues();
            PageSettings.initializeColorValues();

            // Set the data context of the listbox control to the sample data
            this.Loaded += new RoutedEventHandler(MainPage_Loaded);

            // Timer
            DispatcherTimer refreshTimer = new DispatcherTimer();
            refreshTimer.Interval = TimeSpan.FromMinutes(10);
            refreshTimer.Tick += OnRefreshTimerTick;
            refreshTimer.Start();
        }

        void OnRefreshTimerTick(Object sender, EventArgs args)
        {
            webClient_StartDownload();
        }

        // Load data for the ViewModel Items
        private void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            webClient_StartDownload();
        }

        private void refresh_Click(object sender, EventArgs e)
        {
            webClient_StartDownload();
        }


        private void settings_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(
                new Uri("//Settings.xaml", UriKind.Relative));
        }

        private void updateScreenValues()
        {
            double speed = 0;
            double gust = 0;
            double direction = 0;
            double temp = 0;
            int hue = 0;

            textTime.Text = "Mitattu klo " + last_time.ToString("HH:mm");


            // Wind speed
            try 
            {
                speed = timeline_speed[last_time];
                textWind.Text = speed.ToString("F1");
                hue = PageSettings.getHue(speed, PageSettings.color_wind_med1, PageSettings.color_wind_med2, PageSettings.color_wind_absmin, PageSettings.color_wind_absmax);
                textWind.Foreground = new SolidColorBrush(PageSettings.ColorFromHSL(255, hue, 1, 0.6));
            }
            catch (KeyNotFoundException e) 
            {
                textWind.Foreground = new SolidColorBrush(Colors.DarkGray);
                textWind.Text = "---"; 
            }

            // Gust
            try
            {
                gust = timeline_gust[last_time];
                textGust.Text = gust.ToString("F1");
                hue = PageSettings.getHue(gust, PageSettings.color_wind_med1, PageSettings.color_wind_med2, PageSettings.color_wind_absmin, PageSettings.color_wind_absmax);
                textGust.Foreground = new SolidColorBrush(PageSettings.ColorFromHSL(255, hue, 1, 0.6));
            }
            catch (KeyNotFoundException e)
            {
                textGust.Foreground = new SolidColorBrush(Colors.DarkGray);
                textGust.Text = "---";
            }

            // Temperature
            try
            {
                temp = timeline_temp[last_time];
                textTemp.Text = temp.ToString(temp <= -10.0 ? "F0" : "F1");
                hue = PageSettings.getHue(temp, PageSettings.color_temp_med1, PageSettings.color_temp_med2, PageSettings.color_temp_absmin, PageSettings.color_temp_absmax);
                textTemp.Foreground = new SolidColorBrush(PageSettings.ColorFromHSL(255, hue, 1, 0.6));
            }
            catch (KeyNotFoundException e)
            {
                textTemp.Foreground = new SolidColorBrush(Colors.DarkGray);
                textTemp.Text = "---";
            }

            // Direction
            try
            {
                direction = timeline_direction[last_time];
                RotateTransform rotateTransform = new RotateTransform();
                rotateTransform.Angle = direction + 90.0;
                textArrow.RenderTransform = rotateTransform;
                textArrow.Visibility = Visibility.Visible;
                double colorDirection = direction;
                if (direction > 180)
                    colorDirection = 360 - direction;

                textExp.Text = "►";

                hue = PageSettings.getHue(colorDirection, 90, 135, 0, 180);
                textArrow.Foreground = new SolidColorBrush(PageSettings.ColorFromHSL(255, hue, 1, 0.6));
                textExp.Foreground = new SolidColorBrush(PageSettings.ColorFromHSL(255, hue, 1, 0.6));

                if (direction >= 337 || direction < 23)
                    textExp.Text = "Pohjoisesta";
                else if (direction >= 23 && direction < 68)
                    textExp.Text = "Koillisesta";
                else if (direction >= 68 && direction < 113)
                    textExp.Text = "Idästä";
                else if (direction >= 113 && direction < 158)
                    textExp.Text = "Kaakosta";
                else if (direction >= 158 && direction < 203)
                    textExp.Text = "Etelästä";
                else if (direction >= 203 && direction < 248)
                    textExp.Text = "Lounaasta";
                else if (direction >= 248 && direction < 293)
                    textExp.Text = "Lännestä";
                else if (direction >= 293 && direction < 338)
                    textExp.Text = "Luoteesta";

            }
            catch (KeyNotFoundException e)
            {
                textArrow.Text = "";
                textExp.Text = "";
            }

            // Maximum values
            maxGust.Text = mes_maxGust.ToString("F1")+ " m/s";
            maxGustTime.Text = "klo " + mes_maxGustTime.ToString("HH:mm");
            maxSpeed.Text = mes_maxSpeed.ToString("F1")+ " m/s";
            maxSpeedTime.Text = "klo " + mes_maxSpeedTime.ToString("HH:mm");
            maxTemp.Text = mes_maxTemp.ToString("F1")+ " °C";
            maxTempTime.Text = "klo " + mes_maxTempTime.ToString("HH:mm");
            minTemp.Text = mes_minTemp.ToString("F1")+ " °C";
            minTempTime.Text = "klo " + mes_minTempTime.ToString("HH:mm");

            // Dynamic color
            hue = PageSettings.getHue(mes_maxGust, 5.0, 10.0, 0.0, 15.0);
            maxGust.Foreground = new SolidColorBrush(PageSettings.ColorFromHSL(255, hue, 1, 0.6));
            hue = PageSettings.getHue(mes_maxSpeed, 5.0, 10.0, 0.0, 15.0);
            maxSpeed.Foreground = new SolidColorBrush(PageSettings.ColorFromHSL(255, hue, 1, 0.6));
            hue = PageSettings.getHue(mes_maxTemp, 0.0, 20.0, -10.0, 30.0);
            maxTemp.Foreground = new SolidColorBrush(PageSettings.ColorFromHSL(255, hue, 1, 0.6));
            hue = PageSettings.getHue(mes_minTemp, 0.0, 20.0, -10.0, 30.0);
            minTemp.Foreground = new SolidColorBrush(PageSettings.ColorFromHSL(255, hue, 1, 0.6));

            // Draw wind chart
            set_chart_wind();
        }



        private void clearScreenValues()
        {
            textTime.Text = "Päivitetään...";
            textWind.Text = "";
            textGust.Text = "";
            //textDir.Text = "";
            textExp.Text = "";
            textTemp.Text = "";
            textArrow.Visibility = Visibility.Collapsed;

            maxGust.Text = "";
            maxGustTime.Text = "";
            maxSpeed.Text = "";
            maxSpeedTime.Text = "";
            maxTemp.Text = "";
            maxTempTime.Text = "";
            minTemp.Text = "";
            minTempTime.Text = "";

            mes_maxGust = Double.MinValue;
            mes_maxGustTime = DateTime.MinValue;
            mes_maxSpeed = Double.MinValue;
            mes_maxSpeedTime = DateTime.MinValue;
            mes_maxTemp = Double.MinValue;
            mes_maxTempTime = DateTime.MinValue;
            mes_minTemp = Double.MaxValue;
            mes_minTempTime = DateTime.MinValue;
        }

        private void parse_rawdata(String raw_str)
        {
            Double measure_value = 0;
            DateTime measure_time = new DateTime();
            measure_type parsing_state = measure_type.notparsing;
            bool valid_time = false;
            first_time = DateTime.MaxValue;
            last_time = DateTime.MinValue;

            string[] lines = raw_str.Split('\n');
            foreach (string s in lines)
            {
                switch (parsing_state)
                {
                    case measure_type.notparsing:
                        {
                            // Are we onto measre series?
                            if(!s.Contains("<wml2:MeasurementTimeseries"))
                                break;

                            // And what data is coming next?
                            if (s.Contains("ws_10min")) 
                                parsing_state = measure_type.speed;
                            else if (s.Contains("wg_10min")) 
                                parsing_state = measure_type.gust;
                            else if (s.Contains("wd_10min")) 
                                parsing_state = measure_type.direction;
                            else if (s.Contains("t2m")) 
                                parsing_state = measure_type.temp;
                            break;
                        }
                    case measure_type.temp:
                    case measure_type.speed:
                    case measure_type.gust:
                    case measure_type.direction:
                        {
                            // Measure serie ends?
                            if(s.Contains("</wml2:MeasurementTimeseries"))
                            {
                                parsing_state = measure_type.notparsing;
                                valid_time = false;
                                break;
                            }

                            // Do we have time value?
                            if(s.Contains("wml2:time"))
                            {
                                string time = s.Replace("<wml2:time>", "").Replace("</wml2:time>", "").Trim();
                                try
                                {
                                    measure_time = Convert.ToDateTime(time);
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine("Exception caught while parsing XML: " + e.ToString());
                                    break;
                                }
                                valid_time = true;
                            }
                            else if (s.Contains("wml2:value"))
                            {
                                String value = s.Replace("<wml2:value>", "").Replace("</wml2:value>", "").Trim();
                                if (value == "NaN" || !valid_time) break;

                                try
                                {
                                    measure_value = Double.Parse(value, System.Globalization.CultureInfo.InvariantCulture);
                                }
                                catch(Exception e)
                                {
                                    Console.WriteLine("Exception caught while parsing XML: "+e.ToString());
                                    break;
                                }
  
                                if (parsing_state == measure_type.temp)
                                {
                                    timeline_temp[measure_time] = measure_value;
                                    if (measure_value > mes_maxTemp)
                                    {
                                        mes_maxTemp = measure_value;
                                        mes_maxTempTime = measure_time;
                                    }
                                    if (measure_value < mes_minTemp)
                                    {
                                        mes_minTemp = measure_value;
                                        mes_minTempTime = measure_time;
                                    }
                                }
                                else if (parsing_state == measure_type.speed)
                                {
                                    timeline_speed[measure_time] = measure_value;
                                    if (measure_value > mes_maxSpeed)
                                    {
                                        mes_maxSpeed = measure_value;
                                        mes_maxSpeedTime = measure_time;
                                    }
                                }
                                else if (parsing_state == measure_type.gust)
                                {
                                    timeline_gust[measure_time] = measure_value;
                                    if (measure_value > mes_maxGust)
                                    {
                                        mes_maxGust = measure_value;
                                        mes_maxGustTime = measure_time;
                                    }
                                }
                                else if (parsing_state == measure_type.direction)
                                {
                                    timeline_direction[measure_time] = measure_value;
                                }

                                if (measure_time < first_time) first_time = measure_time;
                                if (measure_time > last_time)  last_time  = measure_time;

                                valid_time = false;
                                break;
                            }

                            break;
                        } // case

                } //switch

            } //foreach

            return;
        } //class

        private void set_chart_wind()
        {
            double windMax = 10;
            double tempMax = -100;
            double tempMin = 100;
            windDataCollection.Clear();
            tempDataCollection.Clear();

            foreach (DateTime time in timeline_speed.Keys)
            {
                WindData windData = new WindData();
                windData.Time = time;
                windData.Gust = timeline_gust[time];
                windData.Speed = timeline_speed[time];
                windDataCollection.Add(windData);
                if (windData.Gust > windMax) windMax = windData.Gust;

                TempData tempData = new TempData();
                tempData.Time = time;
                tempData.Temp = timeline_temp[time];
                tempDataCollection.Add(tempData);
                Console.WriteLine(time.ToString() + tempData.Temp.ToString());
                if (tempData.Temp > tempMax) tempMax = tempData.Temp;
                if (tempData.Temp < tempMin) tempMin = tempData.Temp;
            
            }

            double limit = Convert.ToInt32(Math.Ceiling(windMax / 5.0)) * 5.0;
            LinearAxis windY = (LinearAxis)this.windChart.VerticalAxis;
            windY.Maximum = limit;

            limit = Convert.ToInt32(Math.Ceiling(tempMax / 10.0)) * 10.0;
            LinearAxis tempY = (LinearAxis)this.tempChart.VerticalAxis;
            tempY.Maximum = limit;

            limit = Convert.ToInt32(Math.Floor(tempMin / 10.0)) * 10.0;
            tempY = (LinearAxis)this.tempChart.VerticalAxis;
            tempY.Minimum = limit;

            LineSeries series0 = (LineSeries)this.windChart.Series[0];
            LineSeries series1 = (LineSeries)this.windChart.Series[1];

            series0.CategoryBinding = new PropertyNameDataPointBinding() { PropertyName = "Time" };
            series0.ValueBinding = new GenericDataPointBinding<WindData, double>() { ValueSelector = windData => windData.Gust };
            series0.ItemsSource = windDataCollection;

            series1.CategoryBinding = new PropertyNameDataPointBinding() { PropertyName = "Time" };
            series1.ValueBinding = new GenericDataPointBinding<WindData, double>() { ValueSelector = windData => windData.Speed };
            series1.ItemsSource = windDataCollection;

            LineSeries series2 = (LineSeries)this.tempChart.Series[0];
            series2.CategoryBinding = new PropertyNameDataPointBinding() { PropertyName = "Time" };
            series2.ValueBinding = new GenericDataPointBinding<TempData, double>() { ValueSelector = TempData => TempData.Temp };
            series2.ItemsSource = tempDataCollection;

            
        }


        void webClient_StartDownload()
        {
            clearScreenValues();

            webClient = new WebClient();
            webClient.DownloadStringCompleted += new DownloadStringCompletedEventHandler(webClient_DownloadStringCompleted);
            webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(webClient_DownloadProgressChanged);
            webClient.DownloadStringAsync(new Uri(downloadUrl));

            Console.WriteLine("Downloading source from " + downloadUrl);
        }

        void webClient_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            Console.WriteLine("Downloaded " + e.BytesReceived + "/" + e.TotalBytesToReceive + "bytes, " + e.ProgressPercentage + "% completed.");
        }

        void webClient_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            if (e.Cancelled == true)
            {
                Console.WriteLine("Download cancelled.");
                return;
            }

            if (e.Error != null)
            {
                Console.WriteLine("Download error:" + e.Error.ToString());
                return;
            }

            Console.WriteLine("Download completed.");
            parse_rawdata(e.Result);
            updateScreenValues();
        }

    }
}