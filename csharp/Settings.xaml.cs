﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.IO.IsolatedStorage;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace siilinkari
{

    public partial class PageSettings : PhoneApplicationPage
    {
        public static Double color_wind_absmin { get; set; }
        public static Double color_wind_med1 { get; set; }
        public static Double color_wind_med2 { get; set; }
        public static Double color_wind_absmax { get; set; }

        public static Double color_temp_absmin { get; set; }
        public static Double color_temp_med1 { get; set; }
        public static Double color_temp_med2 { get; set; }
        public static Double color_temp_absmax { get; set; }

        private Double wind_step = 0.5;


        public PageSettings()
        {
            InitializeComponent();
            loadSettings();
            createColorMeter();
        }

        private void SettingsPage_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void SettingsPage_Unloaded(object sender, RoutedEventArgs e)
        {
            saveSettings();
        }


        public static void initializeColorValues()
        {
            color_wind_absmin = 0;
            color_wind_med1 = 5;
            color_wind_med2 = 10;
            color_wind_absmax = 20;

            color_temp_absmin = -10;
            color_temp_med1 = 0;
            color_temp_med2 = 20;
            color_temp_absmax = 30;
        }

        private void createColorMeter()
        {
            for (double i = color_wind_absmax; i >= color_wind_absmin; i = i - wind_step)
            {
                Rectangle r = new Rectangle();
                r.Name = i.ToString();
                r.Width = 30;
                r.Width = 60;
                r.Height = 20 * wind_step;
                r.HorizontalAlignment = HorizontalAlignment.Stretch;
                r.MouseEnter += colorTouched;
                ColorPanel.Children.Add(r);
            }
            updateColorMeter();
        }

        public void updateColorMeter()
        {
            try{
                foreach (Rectangle r in ColorPanel.Children)
                {
                    double i = Convert.ToDouble(r.Name);
                    double bright = 0.6;
                    int hue = getHue(i, color_wind_med1, color_wind_med2, color_wind_absmin, color_wind_absmax);
                    SolidColorBrush b = new SolidColorBrush();

                    if (i == color_wind_med1)
                    {
                        TextBlock t = (TextBlock)SettingsPage.FindName("LabelMed1");
                        t.Text = String.Format("{0:#0.0}", i);
                        bright = 0.45;
                    }
                    else if (i == color_wind_med2)
                    {
                        TextBlock t = (TextBlock)SettingsPage.FindName("LabelMed2");
                        t.Text = String.Format("{0:#0.0}", i);
                        bright = 0.5;
                    }

                    b.Color = ColorFromHSL(255, hue, 1.0, bright);
                    r.Fill = b;
                }
            }
            catch(Exception e)
            {
            }

        }

        public void colorTouched(object obj, RoutedEventArgs e)
        {
            double i = Convert.ToDouble(((Rectangle)obj).Name);
            if (i == color_wind_absmin || i == color_wind_absmax)
                return;

            if (Math.Abs(color_wind_med1 - i) < Math.Abs(color_wind_med2 - i))
            {
                color_wind_med1 = i;
                TextBlock t = (TextBlock)SettingsPage.FindName("LabelMed1");
                t.Text = String.Format("{0:F1}", i);
            }
            else
            {
                color_wind_med2 = i;
                TextBlock t = (TextBlock)SettingsPage.FindName("LabelMed2");
                t.Text = String.Format("{0:F1}", i);
            }

            updateColorMeter();
        }

        public void loadSettings()
        {
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;

            if(settings.Contains("wind_med1"))
                color_wind_med1 = (Double)settings["wind_med1"];

            if(settings.Contains("wind_med2"))
                color_wind_med2 = (Double)settings["wind_med2"];

        }

        public void saveSettings()
        {
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;

            if (!settings.Contains("wind_med1"))
                settings.Add("wind_med1", color_wind_med1);
            else
                settings["wind_med1"] = color_wind_med1;

            if (!settings.Contains("wind_med2"))
                settings.Add("wind_med2", color_wind_med2);
            else
                settings["wind_med2"] = color_wind_med2;

            settings.Save();
        }

        public static int getHue(double value, double med1, double med2, double abs_min, double abs_max)
        {
            int hue = 270;

            if (med1 == abs_min || med2 == med1 || abs_max == med2)
                return hue;

            const int hue_min = 240;
            const int hue_med1 = 120;
            const int hue_med2 = 30;
            const int hue_max = 
                0;

            if (value < abs_min) value = abs_min;
            if (value > abs_max) value = abs_max;

            if (value < med1)
            {
                hue = hue_min - Convert.ToInt16((hue_min - hue_med1) * ((value - abs_min) / (med1 - abs_min)));
            }
            else if (value < med2)
            {
                hue = hue_med1 - Convert.ToInt16((hue_med1 - hue_med2) * ((value - med1) / (med2 - med1)));
            }
            else
            {
                hue = hue_med2 - Convert.ToInt16((hue_med2 - hue_max) * ((value - med2) / (abs_max - med2)));
            }
            Console.WriteLine("Hue: {0}", hue);
            return hue;
        }

        public static Color ColorFromHSL(byte alpha, int h, double S, double L)
        {
            double r = 0, g = 0, b = 0;
            double temp1, temp2;
            double H = h / 360.0;

            if (L == 0)
            {
                r = g = b = 0;
            }
            else
            {
                if (S == 0)
                {
                    r = g = b = L;
                }
                else
                {
                    temp2 = ((L <= 0.5) ? L * (1.0 + S) : L + S - (L * S));
                    temp1 = 2.0 * L - temp2;

                    double[] t3 = new double[] { H + 1.0 / 3.0, H, H - 1.0 / 3.0 };
                    double[] clr = new double[] { 0, 0, 0 };
                    for (int i = 0; i < 3; i++)
                    {
                        if (t3[i] < 0)
                            t3[i] += 1.0;
                        if (t3[i] > 1)
                            t3[i] -= 1.0;

                        if (6.0 * t3[i] < 1.0)
                            clr[i] = temp1 + (temp2 - temp1) * t3[i] * 6.0;
                        else if (2.0 * t3[i] < 1.0)
                            clr[i] = temp2;
                        else if (3.0 * t3[i] < 2.0)
                            clr[i] = (temp1 + (temp2 - temp1) * ((2.0 / 3.0) - t3[i]) * 6.0);
                        else
                            clr[i] = temp1;
                    }
                    r = clr[0];
                    g = clr[1];
                    b = clr[2];
                }
            }
            return Color.FromArgb(alpha, (byte)(255 * r), (byte)(255 * g), (byte)(255 * b));
        }




    }
}