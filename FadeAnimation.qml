// Copyright (C) 2019 Pasi Kaukinen

// This file is part of Siilinkarin tuulet.

// Siilinkarin tuulet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Siilinkarin tuulet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Siilinkarin tuulet.  If not, see <https://www.gnu.org/licenses/>.

import QtQuick

SequentialAnimation {
    id: aniroot
    property QtObject target
    property string fadeProperty: "opacity"
    property int fadeDuration: 500
    property alias outValue: outAnimation.to
    property alias inValue: inAnimation.to
    property alias outEasingType: outAnimation.easing.type
    property alias inEasingType: inAnimation.easing.type
    property string easingType: "Quad"
    NumberAnimation {
        id: outAnimation
        target: aniroot.target
        property: aniroot.fadeProperty
        duration: 0 // root.fadeDuration
        to: 0
        easing.type: Easing["In"+aniroot.easingType]
    }
    //PropertyAnimation { target: root.target; property: opacity; to: 0}
    PropertyAction { }
    NumberAnimation {
        id: inAnimation
        target: aniroot.target
        property: aniroot.fadeProperty
        duration: aniroot.fadeDuration
        to: 1
        easing.type: Easing["Out"+aniroot.easingType]
    }
}
