// Copyright (C) 2019 Pasi Kaukinen

// This file is part of Siilinkarin tuulet.

// Siilinkarin tuulet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Siilinkarin tuulet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Siilinkarin tuulet.  If not, see <https://www.gnu.org/licenses/>.


#include <QDebug>
#include <QQmlContext>
#include <QSettings>
#include <QStandardPaths>
#include <QLocale>
#include <QWidget>

#include "poller.h"
#include "application.h"

static const QUrl mainurl(QStringLiteral("qrc:/main.qml"));


Application::Application(int &argc, char **argv) : QApplication(argc, argv),
    m_pEngine(nullptr), m_pPoller(nullptr), m_pRootObject(nullptr), m_bRunning(false), m_pTranslator(nullptr),
    m_WindSpeed(-1), m_WindGust(-1), m_WindDir(-1), m_Temp(-256),
    m_minTemp(-256), m_maxTemp(256), m_maxSpeed(-1), m_maxGust(-1)
{
    // Create QML engine
    m_pEngine = new QQmlApplicationEngine(this);
    connect(m_pEngine, SIGNAL(objectCreated(QObject*, const QUrl&)),
            this, SLOT(onObjectCreated(QObject*, const QUrl&)), Qt::QueuedConnection);
    m_pEngine->rootContext()->setContextProperty("app", this);
    m_pEngine->load(mainurl);

    // Create Poller
    m_pPoller = new Poller(this);
    connect(m_pPoller, SIGNAL(windChanged(double, double, int, double, QDateTime)),
            this, SLOT(onWindChanged(double, double, int, double, QDateTime)));

    connect(m_pPoller, SIGNAL(mesuresChanged()),
            this, SLOT(onMesuresChanged()));

    connect(m_pPoller, SIGNAL(networkError(int,QString)),
            this, SLOT(onNetworkError(int,QString)));

    connect(this, SIGNAL(applicationStateChanged(Qt::ApplicationState)),
            this, SLOT(onApplicationStateChanged(Qt::ApplicationState)));

    // Translator
    m_pTranslator = new QTranslator(this);
    this->installTranslator(m_pTranslator);
}

Application::~Application()
{
    m_pEngine->deleteLater();
    m_pEngine = nullptr;
    m_pPoller->deleteLater();
    m_pPoller = nullptr;
    m_pTranslator->deleteLater();
    m_pTranslator = nullptr;
}

void Application::onObjectCreated(QObject* obj, const QUrl& objUrl)
{
    if (!obj && mainurl == objUrl)
    {
        qDebug() << "Object creation FAILED"<< objUrl.toString();
        QCoreApplication::exit(-1);
        return;
    }
    m_pRootObject = obj;
    qDebug() << "Object created"<< objUrl.toString();
    readSettings();
    connect(m_pRootObject, SIGNAL(settingsChanged(QString, QVariant)), this, SLOT(onSettingsValueChanged(QString, QVariant)));
    connect(m_pRootObject, SIGNAL(refreshRequest()), this, SLOT(onPollRequested()));
    m_pRootObject->setProperty("app_file", appFile());
}

void Application::onPollRequested()
{
    int iForecast = settingsValue("forecast").toInt();
    m_pPoller->poll(iForecast);
}


void Application::onSettingsValueChanged(QString strKey, QVariant tValue)
{
    if(!m_bRunning)
        return;

    //qDebug() << "onSettingsValueChanged " << strKey << tValue;
    QSettings tSettings(settingsFile(), QSettings::IniFormat);
    tSettings.setValue(strKey, tValue);
    tSettings.sync();
    if(m_pRootObject)
        m_pRootObject->setProperty(strKey.toLatin1(), tValue);

    if(strKey=="ui_language")
    {
        setLanguage(tValue.toString());
    }
}

void Application::onNetworkError(int iCode, QString strError)
{
    Q_UNUSED(strError)

    if(!m_pRootObject)
        return;

    QString str1 = tr("Virhe tiedonsiirrossa");
    QString str2 = tr("(Koodi %1)").arg(iCode);

    QMetaObject::invokeMethod(m_pRootObject, "networkError",
                              Q_ARG(QVariant, QVariant(str1)),
                              Q_ARG(QVariant, QVariant(str2))
                              );
}

void Application::setLanguage(QString strLanguage)
{
    qDebug() << "Setting language to " << strLanguage;

    bool bRet = m_pTranslator->load(":/languages/siilinkari_"+strLanguage);
    if(bRet)
        m_pEngine->retranslate();
    else
        qDebug() << "Loading translation file failed.";
}

QString Application::settingsFile()
{
    QString strPath = QStandardPaths::writableLocation(QStandardPaths::ConfigLocation);
    QString strFile = "siilinkarintuulet.ini";
    return strPath + "/" + strFile;
}

void Application::onApplicationStateChanged(Qt::ApplicationState tState)
{
    if(m_pPoller && tState == Qt::ApplicationActive)
    {
        m_bRunning = true;
        m_pPoller->checkPollNeed(settingsValue("forecast").toInt());
    }
}

void Application::onWindChanged(double dSpeed, double dGust, int iDir, double dTemp, QDateTime tTime)
{
    setWindSpeed(dSpeed);
    setWindGust(dGust);
    setWindDir(iDir);
    setTemp(dTemp);
    setWindTime(tTime);

    if(m_pRootObject)
    {
        QMetaObject::invokeMethod(m_pRootObject, "update");
        processEvents();
    }

    qDebug() << "=======\n"
             << qPrintable(tTime.time().toString("HH:mm")) << "\n"
             << dGust <<"\n"
             << dSpeed <<"\n"
             << iDir << "\n"
             << dTemp <<"°C\n=======\n" ;
}

QStringList Application::settingsGeneralKeys()
{
    QStringList tKeys = QStringList()
            << "ui_theme"
            << "ui_language"
            << "station_id"
            << "station_name"
            << "forecast"
               ;
    return tKeys;
}

QStringList Application::settingsWindKeys()
{
    QStringList tKeys = QStringList()
            << "hue_wind_min"
            << "hue_wind_mid1"
            << "hue_wind_mid2"
            << "hue_wind_mid3"
            << "hue_wind_mid4"
            << "hue_wind_max";
    return tKeys;
}

QStringList Application::settingsTempKeys()
{
    QStringList tKeys = QStringList()
            << "hue_temp_min"
            << "hue_temp_mid1"
            << "hue_temp_mid2"
            << "hue_temp_mid3"
            << "hue_temp_mid4"
            << "hue_temp_max";
    return tKeys;
}

QStringList Application::settingsKeys()
{
    return QStringList()
           << settingsWindKeys()
           << settingsTempKeys()
           << settingsGeneralKeys()
           << "hue_wind_test" << "hue_temp_test";
}

QVariant Application::defaultValue(QString strKey)
{
    QVariant tRet = 0;
    if(strKey=="hue_wind_min") tRet = 0;
    else if(strKey=="hue_wind_mid1") tRet = 6;
    else if(strKey=="hue_wind_mid2") tRet = 8;
    else if(strKey=="hue_wind_mid3") tRet = 10;
    else if(strKey=="hue_wind_mid4") tRet = 12;
    else if(strKey=="hue_wind_max") tRet = 20;
    else if(strKey=="hue_temp_min") tRet = 0;
    else if(strKey=="hue_temp_mid1") tRet = 10;
    else if(strKey=="hue_temp_mid2") tRet = 15;
    else if(strKey=="hue_temp_mid3") tRet = 20;
    else if(strKey=="hue_temp_mid4") tRet = 25;
    else if(strKey=="hue_temp_max") tRet = 30;
    else if(strKey=="hue_wind_test") tRet = 0;
    else if(strKey=="hue_temp_test") tRet = 0;
    else if(strKey=="ui_theme") tRet = 0;
    else if(strKey=="ui_language") tRet = defaultLanguage();
    else if(strKey=="station_id") tRet = 101311;
    else if(strKey=="station_name") tRet = tr("Siilinkari");
    else if(strKey=="forecast") tRet = 0;
    return tRet;
}

QString Application::defaultLanguage()
{
    QLocale loc;
    QString str = loc.uiLanguages().first().split("-").first();
    QStringList tSupported = QStringList() << "fi" << "en" << "se";
    if(!tSupported.contains(str))
    {
        qDebug() << "System language " + str + " not supported. Defaulting to en.";
        str = "en";
    }
    return str;
}

QVariant Application::settingsValue(QString key, QVariant tVar)
{
    QString strIniFile = settingsFile();
    QSettings tSettings(strIniFile, QSettings::IniFormat);
    return tSettings.value(key, tVar);
}

void Application::readSettings()
{
    QString strIniFile = settingsFile();
    qDebug() << "Using settings " << strIniFile;
    QSettings tSettings(strIniFile, QSettings::IniFormat);
    QStringList tKeys = settingsKeys();
    if(!m_pRootObject )
    {
        qDebug() << "No root object!";
        return;
    }

    foreach(QString strKey, tKeys)
    {
        m_pRootObject->setProperty(strKey.toLatin1(), tSettings.value(strKey, defaultValue(strKey)));
    }

    // Set Language
    QString strLanguage = tSettings.value("ui_language", defaultLanguage()).toString();
    setLanguage(strLanguage);
    m_pRootObject->setProperty("stabilized", true);
}

void Application::onMesuresChanged()
{
    if(!m_pPoller || !m_pRootObject )
        return;

    m_pRootObject->setProperty("refresh_requested",false);

    setMinTemp(m_pPoller->minTemp());
    setMinTempTime(m_pPoller->minTempTime());
    setMaxTemp(m_pPoller->maxTemp());
    setMaxTempTime(m_pPoller->maxTempTime());

    setMaxSpeed(m_pPoller->maxWind());
    setMaxSpeedTime(m_pPoller->maxWindTime());
    setMaxGust(m_pPoller->maxGust());
    setMaxGustTime(m_pPoller->maxGustTime());

    int iForecast = settingsValue("forecast", 0).toInt();

    QObject* pWindObject = m_pRootObject->findChild<QObject*>("chartview");
    if(!pWindObject)
        return;

    QObject* pTempObject = m_pRootObject->findChild<QObject*>("tempchartview");
    if(!pTempObject)
        return;


    QList<QDateTime> tMeasureTimes = m_pPoller->measureTimes();
    if(tMeasureTimes.count()==0)
    {
        return;
    }

    QList<QDateTime> tForecastTimes = m_pPoller->forecastTimes();


    QDateTime tLast = tMeasureTimes.last();
    int iMinutes = tLast.time().minute() % 30;
    if(iMinutes>0)
        tLast = tLast.addSecs((30-iMinutes)*60);
    if(iForecast>0)
        tLast = tLast.addSecs(iForecast*3600);

    QMetaObject::invokeMethod(pWindObject, "resetMeasures",
                              Q_ARG(QVariant, QVariant(tLast.addSecs(-(12+iForecast)*3600))),
                              Q_ARG(QVariant, QVariant(tLast)),
                              Q_ARG(QVariant, QVariant(qMax(maxSpeed(), m_pPoller->maxWindForecast() ))),
                              Q_ARG(QVariant, QVariant(qMax(maxGust(), m_pPoller->maxGustForecast() )))
                              );

    QMetaObject::invokeMethod(pTempObject, "resetMeasures",
                              Q_ARG(QVariant, QVariant(tLast.addSecs(-(12+iForecast)*3600))),
                              Q_ARG(QVariant, QVariant(tLast)),
                              Q_ARG(QVariant, QVariant(qMin(minTemp(),m_pPoller->minTempForecast()))),
                              Q_ARG(QVariant, QVariant(qMax(maxTemp(),m_pPoller->maxTempForecast())))
                              );

    int iCount = 0;
    double dWind = 0;
    double dGust = 0;
    int iDir = 0;
    double dTemp = 0;
    foreach(QDateTime dt, tMeasureTimes)
    {
        iCount++;
        dWind += m_pPoller->measuredWind(dt);
        double dGustLandscape = m_pPoller->measuredGust(dt);
        if(dGustLandscape>dGust)
            dGust = dGustLandscape;
        iDir += m_pPoller->measuredDir(dt);
        dTemp += m_pPoller->measuredTemp(dt);

        QMetaObject::invokeMethod(pWindObject, "addMeasureLandscape",
                                  Q_ARG(QVariant, QVariant(dt)),
                                  Q_ARG(QVariant, QVariant(m_pPoller->measuredWind(dt))),
                                  Q_ARG(QVariant, QVariant(dGustLandscape)),
                                  Q_ARG(QVariant, QVariant(m_pPoller->measuredDir(dt)))
                                  );

        QMetaObject::invokeMethod(pTempObject, "addMeasureLandscape",
                                  Q_ARG(QVariant, QVariant(dt)),
                                  Q_ARG(QVariant, QVariant(m_pPoller->measuredTemp(dt)))
                                  );


        if(dt==tMeasureTimes.first() || dt==tMeasureTimes.last() || dt.time().minute()==0)
        {
            QMetaObject::invokeMethod(pWindObject, "addMeasure",
                                      Q_ARG(QVariant, QVariant(dt)),
                                      Q_ARG(QVariant, QVariant(dWind/iCount)),
                                      Q_ARG(QVariant, QVariant(dGust)),
                                      Q_ARG(QVariant, QVariant(iDir/iCount))
                                      );
            QMetaObject::invokeMethod(pTempObject, "addMeasure",
                                      Q_ARG(QVariant, QVariant(dt)),
                                      Q_ARG(QVariant, QVariant(dTemp/iCount))
                                      );
            iCount = 0;
            dWind = 0;
            dGust = 0;
            iDir = 0;
            dTemp = 0;
        }
    }

    if(iForecast>0)
    {
        for(int i=0; i<=iForecast*2; i++)
        {

            if(i<tForecastTimes.count())
            {
                QDateTime dt = tForecastTimes.at(i);
                if(dt <= tLast)
                {
                    double dWind = m_pPoller->forecastedWind(dt);
                    double dGust = m_pPoller->forecastedGust(dt);
                    int iDir = m_pPoller->forecastedDir(dt);
                    double dTemp = m_pPoller->forecastedTemp(dt);
                    QMetaObject::invokeMethod(pWindObject, "addForecast",
                                              Q_ARG(QVariant, QVariant(dt)),
                                              Q_ARG(QVariant, QVariant(dWind)),
                                              Q_ARG(QVariant, QVariant(dGust)),
                                              Q_ARG(QVariant, QVariant(iDir))
                                              );
                    QMetaObject::invokeMethod(pTempObject, "addForecast",
                                              Q_ARG(QVariant, QVariant(dt)),
                                              Q_ARG(QVariant, QVariant(dTemp))
                                              );
                }
            }
        }
    }
}

QString Application::appFile()
{
    return applicationFilePath();
}
