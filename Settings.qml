// Copyright (C) 2019 Pasi Kaukinen

// This file is part of Siilinkarin tuulet.

// Siilinkarin tuulet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Siilinkarin tuulet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Siilinkarin tuulet.  If not, see <https://www.gnu.org/licenses/>.

import QtQuick
import QtQuick.Controls

Rectangle {
    color: "#303030"
    state: ""
    anchors.fill: parent
    opacity: 1.0

    property alias currentSettingsPage: settingsSwipeView.currentIndex

    Image {
        opacity: 1.0
        source: dark()?"images/settings_dark.png":"images/settings_light.png"
        width: Math.min(parent.width, parent.height) * 0.1
        height: Math.min(parent.width, parent.height) * 0.1
        anchors.top: parent.top
        anchors.topMargin: parent.width*0.02
        anchors.right: parent.right
        anchors.rightMargin: isPortrait() ? parent.width*0.02 : parent.height*0.02
        smooth: true
        z: 93
        fillMode: Image.PreserveAspectFit
        MouseArea {

            anchors.fill: parent
            onClicked: {
                parent.parent.visible = false
            }
        }
    }

    SwipeView {
        id: settingsSwipeView
        anchors.fill: parent
        currentIndex: 0

        SettingsPage1 { id: settings_general }
        SettingsPage2 { id: settings_wind }
        SettingsPage3 { id: settings_temp }
        SettingsPage4 { id: settings_about }
    }

    PageIndicator {
        id: settingsIndicator

        count: settingsSwipeView.count
        currentIndex: settingsSwipeView.currentIndex

        anchors.bottom: settingsSwipeView.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }
}


