// Copyright (C) 2019 Pasi Kaukinen

// This file is part of Siilinkarin tuulet.

// Siilinkarin tuulet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Siilinkarin tuulet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Siilinkarin tuulet.  If not, see <https://www.gnu.org/licenses/>.

import QtQuick
import QtQuick.Controls


Slider {
    id: valueslider
    value: root.hue_wind_test
    width: parent.width * 0.9
    height: parent.height * 0.05
    anchors.left: parent.left
    anchors.leftMargin: parent.width * 0.05
    from: 0
    to: 30
    stepSize: 0.1
    snapMode: Slider.SnapAlways
    property string value_key
    onValueChanged: {
        root.settingsValueChanged(value_key, value)
    }
    background: Rectangle {
        x: valueslider.leftPadding
        y: valueslider.topPadding + valueslider.availableHeight / 2 - height / 2
        implicitWidth: 200
        implicitHeight: 4
        width: valueslider.availableWidth
        height: implicitHeight
        radius: 2
        color: "#808080"
    }
    handle: Rectangle {
        x: valueslider.leftPadding + valueslider.visualPosition * (valueslider.availableWidth - width)
        y: valueslider.topPadding + valueslider.availableHeight / 2 - height / 2
        implicitWidth: 14
        implicitHeight: 14
        radius: 7
        color: dark() ? "#FFFFFF" : "#212121"
        border.color: dark() ? "#E0E0E0" : "#404040"
    }
}
