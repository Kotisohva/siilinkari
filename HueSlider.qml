// Copyright (C) 2019 Pasi Kaukinen

// This file is part of Siilinkarin tuulet.

// Siilinkarin tuulet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Siilinkarin tuulet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Siilinkarin tuulet.  If not, see <https://www.gnu.org/licenses/>.

import QtQuick
import QtQuick.Controls
import QtQuick.Controls.Material

Item {
    property alias value: valueslider.value
    property alias label: huelabel.text
    property alias from: valueslider.from
    property alias to: valueslider.to
    property alias color: huevalue.color
    property alias step: valueslider.stepSize
    property int decimals: 1
    property string key
    width: root.width
    height: root.height * 0.07
    Text {
        id: huelabel
        width: parent.width * 0.25
        height: parent.height
        font.pixelSize: isPortrait() ? height * 0.35 : height * 0.5
        color: Material.foreground
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.leftMargin: 20
        verticalAlignment: Text.AlignVCenter
    }
    Text {
        id: huevalue
        width: parent.width * 0.15
        height: parent.height
        text: valueslider.value.toFixed(decimals)
        font.pixelSize: isPortrait() ? height * 0.4 : height * 0.5
        anchors.left: huelabel.right
        anchors.top: parent.top
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignRight
        font.bold: true
    }
    Slider {
        id: valueslider
        width: parent.width * 0.50
        height: parent.height
        anchors.left: huevalue.right
        anchors.verticalCenter: huevalue.verticalCenter
        from: 0
        to: 30
        stepSize: 0.5
        snapMode: Slider.SnapAlways
        onValueChanged: {
            root.settingsValueChanged(key, value)
        }
        background: Rectangle {
            x: valueslider.leftPadding
            y: valueslider.topPadding + valueslider.availableHeight / 2 - height / 2
            implicitWidth: 200
            implicitHeight: 4
            width: valueslider.availableWidth
            height: implicitHeight
            radius: 2
            color: "#808080"
        }
        handle: Rectangle {
            x: valueslider.leftPadding + valueslider.visualPosition * (valueslider.availableWidth - width)
            y: valueslider.topPadding + valueslider.availableHeight / 2 - height / 2
            implicitWidth: 14
            implicitHeight: 14
            radius: 7
            color: dark() ? "#FFFFFF" : "#212121"
            border.color: dark() ? "#E0E0E0" : "#404040"
        }
    }
}

