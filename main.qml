// Copyright (C) 2019 Pasi Kaukinen

// This file is part of Siilinkarin tuulet.

// Siilinkarin tuulet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Siilinkarin tuulet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Siilinkarin tuulet.  If not, see <https://www.gnu.org/licenses/>.

import QtQuick
import QtQuick.Controls
import QtQuick.Controls.Material

ApplicationWindow {
    id: root
    visible: true
//    width: 800
//    height: 380
    width: 380
    height: 800
    Material.theme: ui_theme===0 ? Material.Dark : Material.Light

    title: qsTr("Siilinkarin tuulet")

    property double hue_wind_min: 0
    property double hue_wind_mid1: 6
    property double hue_wind_mid2: 8
    property double hue_wind_mid3: 10
    property double hue_wind_mid4: 12
    property double hue_wind_max: 20
    property double hue_wind_test: 0

    property double hue_temp_min: 0
    property double hue_temp_mid1: 10
    property double hue_temp_mid2: 15
    property double hue_temp_mid3: 20
    property double hue_temp_mid4: 25
    property double hue_temp_max: 30
    property double hue_temp_test: 20

    property int ui_theme: 0
    property string ui_language: "en"

    property int station_id
    property string station_name
    property int forecast: 12
    property bool stabilized: false

    property alias currentPage: swipeView.currentIndex
    property alias settingsVisible: settings_page.visible

    property bool refresh_requested: false

    property string app_file

    signal settingsChanged(string key, var value)
    signal refreshRequest()

    onActiveChanged: {
        if(active)
            root.update()
    }

    onClosing: {
        if(settingsVisible === true)
        {
            close.accepted = false
            if(settings_page.currentSettingsPage>0)
                settings_page.currentSettingsPage = settings_page.currentSettingsPage - 1
            else
                settingsVisible = false
        }
        else if(currentPage>0)
        {
            close.accepted = false
            currentPage = currentPage - 1
        }
    }

    BusyBalls {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: parent.height * 0.12
        running: root.refresh_requested
        z: 99
    }

    Image {
        id: settings_icon
        opacity: 0.3
        source: dark()?"images/settings_dark.png":"images/settings_light.png"
        width: Math.min(parent.width, parent.height) * 0.1
        height: Math.min(parent.width, parent.height) * 0.1
        anchors.top: parent.top
        anchors.topMargin: parent.width*0.02
        anchors.right: parent.right
        anchors.rightMargin: isPortrait() ? parent.width*0.02 : parent.height*0.02
        smooth: true
        z: 80
        fillMode: Image.PreserveAspectFit
        Behavior on opacity { NumberAnimation { duration: 200; } }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                settingsVisible = true
            }
        }
    }

    Settings {
        id: settings_page
        visible: false
        z: 90
    }

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: 0
        Page1 { id: page_measures }
        Page2 { id: page_highs }
        Page3 { id: page_graphs }
        Page4 { id: page_tempgraphs }
    }

    PageIndicator {
        id: indicator

        count: swipeView.count
        currentIndex: swipeView.currentIndex

        anchors.bottom: swipeView.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Popup {
        id: popup
        anchors.centerIn: parent
        width: parent.width * 0.8
        height: parent.height * 0.1
        modal: true
        focus: true
        visible: false
        topMargin: 0
        property alias poptext: pop_text.text
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside
        Text {
            id: pop_text
            color: Material.foreground
            width: parent.width
            height: parent.height
            font.pointSize: 100
            minimumPointSize: 10
            fontSizeMode: Text.Fit
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
        MouseArea {
            anchors.fill: parent
            onClicked: { popup.visible = false }
        }
    }

    function networkError(str1, str2)
    {
        console.log("onNetworkError: "+str1 +  " " + str2)
        popup.poptext = str1+"<br><font color='grey'>"+str2+"</font>"
        popup.visible = true
    }

    function dark()
    {
        return root.ui_theme === 0
    }

    function light()
    {
        return root.ui_theme === 1
    }

    function settingsValueChanged(key, value)
    {
        if(stabilized===false)
            return

        console.log("Root Settings changed: " + key + " " + value)
        root.settingsChanged(key, value)
    }

    function refresh()
    {
        if(refresh_requested)
            return

        console.log("Requesting refresh")
        refresh_requested = true
        root.refreshRequest()
    }



    function isPortrait()
    {
        return root.height > root.width
    }

    function isLandscape()
    {
        return root.height < root.width
    }

    function getWindDirection(direction)
    {
        var str;
        if (direction < 0)
            str = ""
        else if (direction >= 337 || direction < 23)
            str = qsTr("pohjoisesta")
        else if (direction >= 23 && direction < 68)
            str = qsTr("koillisesta")
        else if (direction >= 68 && direction < 113)
            str = qsTr("idästä")
        else if (direction >= 113 && direction < 158)
            str = qsTr("kaakosta")
        else if (direction >= 158 && direction < 203)
            str = qsTr("etelästä")
        else if (direction >= 203 && direction < 248)
            str = qsTr("lounaasta")
        else if (direction >= 248 && direction < 293)
            str = qsTr("lännestä")
        else if (direction >= 293 && direction < 338)
            str = qsTr("luoteesta")
        return str
    }

    function getWindColor(value)
    {
        var hue = getHue(value, hue_wind_min, hue_wind_mid1, hue_wind_mid2, hue_wind_mid3, hue_wind_mid4, hue_wind_max)
        return  getColor(hue)
    }

    function getTempColor(value)
    {
        var hue = getHue(value, hue_temp_min, hue_temp_mid1, hue_temp_mid2, hue_temp_mid3, hue_temp_mid4, hue_temp_max)
        return getColor(hue)
    }

    function getColor(hue)
    {
      return Qt.hsla(hue, 0.8, dark()?0.6:0.4, 1)
    }

    function getHue(value, abs_min, med1, med2, med3, med4, abs_max)
    {
        var hue = 270;      // Magenta

        // Make sure values are sensible, somehow
        if(abs_min>med1) med1=abs_min+0.01
        if(med1>med2) med2=med1+0.02
        if(med2>med3) med3=med3+0.03
        if(med3>abs_max) abs_max=med3+0.04

        // Make sure there will be no zero divation
        if(abs_min===med1) med1=med1+0.01
        if(med1===med2) med2=med2+0.02
        if(med2===med3) med3=med3+0.03
        if(med3===med4) med4=med4+0.04
        if(med4===abs_max) abs_max=abs_max+0.04

        if (med1 === abs_min || med2 === med1 || med3 === med2 || med4 === med3 ||abs_max === med3)
            return hue; // SHould not never happen but...

        var hue_min = 220;  // Blue
        var hue_med1 = 120; // Green
        var hue_med2 = 60;  // Yellow
        var hue_med3 = 30;  // Orange
        var hue_med4 = 0;   // Red
        var hue_max = -90;  // Magenta

        if (value < abs_min) value = abs_min;
        if (value > abs_max) value = abs_max;

        if (value < med1)
        {
            hue = hue_min - Math.floor((hue_min - hue_med1) * ((value - abs_min) / (med1 - abs_min)));
        }
        else if (value < med2)
        {
            hue = hue_med1 - Math.floor((hue_med1 - hue_med2) * ((value - med1) / (med2 - med1)));
        }
        else if (value < med3)
        {
            hue = hue_med2 - Math.floor((hue_med2 - hue_med3) * ((value - med2) / (med3 - med2)));
        }
        else if (value < med4)
        {
            hue = hue_med3 - Math.floor((hue_med3 - hue_med4) * ((value - med3) / (med4 - med3)));
        }
        else
        {
            hue = hue_med4 - Math.floor((hue_med4 - hue_max) * ((value - med4) / (abs_max - med4)));
            if(hue<0)
                hue+=360
        }
        return hue/360
    }

    function backGroundImage()
    {
        // var b = "qrc:/images/" + root.station_id + ".png"

        var b = "qrc:/images/" + 101311 + ".png"

        return b;
    }
}
