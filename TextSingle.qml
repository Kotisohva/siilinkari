// Copyright (C) 2019 Pasi Kaukinen

// This file is part of Siilinkarin tuulet.

// Siilinkarin tuulet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Siilinkarin tuulet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Siilinkarin tuulet.  If not, see <https://www.gnu.org/licenses/>.

import QtQuick

Item {
    id: textsingle
    property alias text1: idtext.text
    property alias color1: idtext.color
    property alias rotation1: idtext.rotation
    property alias horizontalAlignment1: idtext.horizontalAlignment
    property alias verticalAlignment1: idtext.verticalAlignment
    property alias bold1: idtext.font.bold

    width: isPortrait() ? page_root.width  : page_root.width * 0.5
    height: isPortrait() ? page_root.height * 0.2 : page_root.height * 0.4

    Text {
       id: idtext
       text: ""
       width: parent.width * 0.96
       height: parent.height
       anchors.top: parent.top
       anchors.left: parent.left
       horizontalAlignment: Text.AlignHCenter
       verticalAlignment: Text.AlignVCenter
       fontSizeMode: Text.Fit
       font.pixelSize: height*0.5
       Behavior on text {
           FadeAnimation { target: idtext }
       }
    }
}
