<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fi">
<context>
    <name>Application</name>
    <message>
        <location filename="../application.cpp" line="121"/>
        <source>Virhe tiedonsiirrossa</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../application.cpp" line="122"/>
        <source>(Koodi %1)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../application.cpp" line="244"/>
        <source>Siilinkari</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Page1</name>
    <message>
        <location filename="../Page1.qml" line="55"/>
        <location filename="../Page1.qml" line="65"/>
        <source>m/s</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Page1.qml" line="56"/>
        <source>puuskatuuli</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Page1.qml" line="66"/>
        <source>keskituuli</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Page2</name>
    <message>
        <location filename="../Page2.qml" line="27"/>
        <source>ÄÄRIARVOT</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Page2.qml" line="35"/>
        <source>Kovin puuska</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Page2.qml" line="46"/>
        <source>Keskituuli</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Page2.qml" line="57"/>
        <source>Kylmintä</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Page2.qml" line="68"/>
        <source>Lämpöisintä</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Page3</name>
    <message>
        <location filename="../Page3.qml" line="28"/>
        <source>TUULET</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Page4</name>
    <message>
        <location filename="../Page4.qml" line="28"/>
        <source>LÄMPÖKÄYRÄ</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Poller</name>
    <message>
        <location filename="../poller.cpp" line="206"/>
        <source>Tuorein mittausdata ei saatavilla</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../poller.cpp" line="596"/>
        <source>Host opendata.fmi.fi not found</source>
        <translation>Palvelinta opendata.fmi.fi ei löydy</translation>
    </message>
    <message>
        <location filename="../poller.cpp" line="598"/>
        <source>Network access is disabled.</source>
        <translation>Verkkoyhteys on poistettu käytöstä.</translation>
    </message>
</context>
<context>
    <name>SettingsPage1</name>
    <message>
        <location filename="../SettingsPage1.qml" line="27"/>
        <source>ASETUKSET   1/3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="50"/>
        <source>Mittausasema</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="126"/>
        <source>Tampere Siilinkari</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="81"/>
        <source>Asikkala Pulkkilanharju</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="82"/>
        <source>Hailuoto Marjaniemi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="84"/>
        <source>Hanko Tulliniemi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="83"/>
        <source>Hanko Russarö</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="85"/>
        <source>Helsinki Harmaja</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="86"/>
        <source>Hammarland Märket</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="87"/>
        <source>Helsinki Majakka</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="88"/>
        <source>Inari Seitalaassa</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="89"/>
        <source>Inkoo Bågaskär</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="90"/>
        <source>Kalajoki Ulkokalla</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="91"/>
        <source>Kaskinen Sälgrund</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="92"/>
        <source>Kemi Ajos</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="93"/>
        <source>Kemi I majakka</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="94"/>
        <source>Kemiönsaari Vänö</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="95"/>
        <source>Kirkkonummi Mäkiluoto</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="96"/>
        <source>Kokkola Tankar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="97"/>
        <source>Kotka Haapasaari</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="98"/>
        <source>Kotka Rankki</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="99"/>
        <source>Kuopio Ritoniemi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="100"/>
        <source>Kustavi Isokari</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="101"/>
        <source>Korsnäs Bredskäret</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="102"/>
        <source>Kristiinankaupunki majakka</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="103"/>
        <source>Kumlinge kirkonkylä</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="104"/>
        <source>Kökar Bogskär</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="105"/>
        <source>Lappeenranta Hiekkapakka</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="106"/>
        <source>Lemland Nyhamn</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="107"/>
        <source>Liperi Tuiskavanluoto</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="108"/>
        <source>Lumparland Långnäs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="109"/>
        <source>Luhanka Judinsalo</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="110"/>
        <source>Loviisa Orrengrund</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="111"/>
        <source>Maalahti Strömmingsbådan</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="112"/>
        <source>Mustasaari Valassaaret</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="113"/>
        <source>Oulu Vihreäsaari</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="114"/>
        <source>Parainen Fagerholm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="115"/>
        <source>Parainen Utö</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="116"/>
        <source>Pietarsaari Kallan</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="117"/>
        <source>Pori Tahkoluoto</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="118"/>
        <source>Porvoo Emäsalo</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="119"/>
        <source>Porvoo Kalbådagrund</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="120"/>
        <source>Raasepori Jussarö</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="121"/>
        <source>Rantasalmi Rukkasluoto</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="122"/>
        <source>Raahe Nahkiainen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="123"/>
        <source>Raahe Lapaluoto</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="124"/>
        <source>Rauma Kylmäpihlaja</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="125"/>
        <source>Sipoo Itätoukki</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="127"/>
        <source>Turku Rajakari</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="176"/>
        <source>Ennuste</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="207"/>
        <source>Ei ennustetta</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="208"/>
        <source>3 seuraavaa tuntia</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="209"/>
        <source>6 seuraavaa tuntia</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="210"/>
        <source>12 seuraavaa tuntia</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="235"/>
        <source>Teema</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="250"/>
        <source>Vaalea</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="256"/>
        <source>Tumma</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="262"/>
        <source>Kieli</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="277"/>
        <source>Suomi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="283"/>
        <source>Svenska</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage1.qml" line="289"/>
        <source>English</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SettingsPage2</name>
    <message>
        <location filename="../SettingsPage2.qml" line="26"/>
        <source>ASETUKSET   2/3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage2.qml" line="33"/>
        <source>Tuulen väritys</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage2.qml" line="47"/>
        <source>Sininen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage2.qml" line="56"/>
        <source>Vihreä</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage2.qml" line="65"/>
        <source>Keltainen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage2.qml" line="74"/>
        <source>Oranssi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage2.qml" line="83"/>
        <source>Punainen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage2.qml" line="92"/>
        <source>Violetti</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SettingsPage3</name>
    <message>
        <location filename="../SettingsPage3.qml" line="26"/>
        <source>ASETUKSET   3/3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage3.qml" line="33"/>
        <source>Lämpötilan väritys</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage3.qml" line="47"/>
        <source>Sininen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage3.qml" line="59"/>
        <source>Vihreä</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage3.qml" line="70"/>
        <source>Keltainen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage3.qml" line="81"/>
        <source>Oranssi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage3.qml" line="92"/>
        <source>Punainen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage3.qml" line="103"/>
        <source>Violetti</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SettingsPage4</name>
    <message>
        <location filename="../SettingsPage4.qml" line="26"/>
        <source>TIETOJA</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage4.qml" line="36"/>
        <source>Siilinkarin tuulet</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage4.qml" line="37"/>
        <source>Kotisohvan sovellukset tekee</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage4.qml" line="38"/>
        <source>Mittaustulokset tarjoaa Ilmatieteen laitos</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../SettingsPage4.qml" line="40"/>
        <source>Lisenssi</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../main.qml" line="31"/>
        <source>Siilinkarin tuulet</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main.qml" line="226"/>
        <source>pohjoisesta</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main.qml" line="228"/>
        <source>koillisesta</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main.qml" line="230"/>
        <source>idästä</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main.qml" line="232"/>
        <source>kaakosta</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main.qml" line="234"/>
        <source>etelästä</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main.qml" line="236"/>
        <source>lounaasta</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main.qml" line="238"/>
        <source>lännestä</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main.qml" line="240"/>
        <source>luoteesta</source>
        <translation></translation>
    </message>
</context>
</TS>
