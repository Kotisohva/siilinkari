// Copyright (C) 2019 Pasi Kaukinen

// This file is part of Siilinkarin tuulet.

// Siilinkarin tuulet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Siilinkarin tuulet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Siilinkarin tuulet.  If not, see <https://www.gnu.org/licenses/>.

import QtQuick
import QtQuick.Controls

RadioButton {
    width: isPortrait() ? parent.width * 0.95 : parent.width * 0.45
    anchors.right: parent.right
    height: isPortrait() ? root.height * 0.05 : root.height * 0.1
    font.pixelSize: height*0.5

    checked: root.ui_language === svalue
    ButtonGroup.group: languageGroup

    property string svalue

    onCheckedChanged: {
        if(checked)
        {
            root.ui_language = svalue
            root.settingsValueChanged("ui_language", svalue)
            settings_page.sortLanguages()
            forecast_combobox.findIndex()
        }
    }
}
