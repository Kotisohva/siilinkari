// Copyright (C) 2019 Pasi Kaukinen

// This file is part of Siilinkarin tuulet.

// Siilinkarin tuulet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Siilinkarin tuulet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Siilinkarin tuulet.  If not, see <https://www.gnu.org/licenses/>.

import QtQuick
import QtQuick.Controls
import QtCharts
import QtQuick.Controls.Material

Page {
    width: root.width
    height: root.height
    clip: true
    header: Label {
        text: qsTr("TUULET")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
        bottomPadding: -20
    }

    FlickablePage {
        ChartView {
            id: chartview
            objectName: "chartview"
            title: ""
            anchors.fill: parent
            antialiasing: true
            backgroundColor: "transparent"
            backgroundRoundness: 0
            legend.visible: false

            animationDuration: 500
            animationOptions: ChartView.SeriesAnimations

            property int portraitYticks: 11
            property int landscapeYticks: 6

            onWidthChanged: { setYticks() }

            Behavior on scale { NumberAnimation { duration: 200 } }

            SplineSeries {
                id: sp_gust
                visible: root.isPortrait()
                width: 3
                capStyle: Qt.RoundCap
                style: Qt.SolidLine
                color: "red"
                axisX: DateTimeAxis {
                    format: "hh:mm"
                    labelsColor: dark() ? "#E0E0E0" : "#404040"
                    gridLineColor: dark() ? "#404040" : "#E0E0E0"
                }
                axisY: ValueAxis {
                    labelFormat: "%2u"
                    labelsColor: dark() ? "#E0E0E0" : "#404040"
                    gridLineColor: dark() ? "#404040" : "#E0E0E0"
                    min: 0
                }
            }

            SplineSeries {
                id: sp_gust_landscape
                visible: isLandscape()
                width: 3
                capStyle: Qt.RoundCap
                style: Qt.SolidLine
                color: "red"
                axisX: DateTimeAxis {
                    visible: false
                }
                axisY: ValueAxis {
                    visible: false
                    min: 0
                }
            }

            SplineSeries {
                id: sp_wind
                visible: isPortrait()
                width: 3
                capStyle: Qt.RoundCap
                style: Qt.SolidLine
                color: "yellow"
                axisX: DateTimeAxis {
                    visible: false
                }
            }

            SplineSeries {
                id: sp_wind_landscape
                visible: isLandscape()
                width: 3
                capStyle: Qt.RoundCap
                style: Qt.SolidLine
                color: "yellow"
                axisX: DateTimeAxis {
                    visible: false
                }
                axisY: ValueAxis {
                    visible: false
                    min: 0
                }
            }

            SplineSeries {
                id: sp_dir
                visible: isPortrait()
                width: 15
                capStyle: Qt.RoundCap
                style: Qt.SolidLine
                opacity: 0.16
                color: dark() ? "white" : "black"
                axisXTop: DateTimeAxis {
                    visible: false
                    labelsColor: dark() ? "#E0E0E0" : "#404040"
                    gridLineColor: dark() ? "#404040" : "#E0E0E0"
                }
                axisYRight: ValueAxis {
                    labelFormat: "%3i"
                    labelsColor: dark()? "#606060" : "#C0C0C0"
                    color: dark()? "#606060" : "#C0C0C0"
                    tickCount: 5
                    gridVisible: false
                    reverse: true
                    min: 0
                    max: 360
                }
            }

            SplineSeries {
                id: sp_dir_landscape
                visible: isLandscape()
                width: 15
                capStyle: Qt.RoundCap
                style: Qt.SolidLine
                opacity: 0.16
                color: dark() ? "white" : "black"
                axisXTop: DateTimeAxis {
                    visible: false
                }
                axisYRight: ValueAxis {

                    visible: false
                    gridVisible: false
                    reverse: true
                    min: 0
                    max: 360
                }
            }

            SplineSeries {
                id: sp_wind_forecast
                name: "Keskituuli ennuste"
                width: 3
                capStyle: Qt.RoundCap
                style: Qt.DotLine
                axisX: DateTimeAxis {
                    visible: false
                }
            }

            SplineSeries {
                id: sp_gust_forecast
                name: "Puuskatuuli ennuste"
                width: 3
                capStyle: Qt.RoundCap
                style: Qt.DotLine
                axisX: DateTimeAxis {
                    visible: false
                }
            }

            SplineSeries {
                id: sp_dir_forecast
                name: "Tuulensuunta ennuste"
                width: 10
                capStyle: Qt.RoundCap
                style: Qt.DotLine
                opacity: 0.16
                color: dark() ? "white" : "black"
                axisX: DateTimeAxis {
                    visible: false
                }
                axisY: ValueAxis {
                    visible: false
                    reverse: true
                    min: 0
                    max: 360
                }
            }

            function setYticks()
            {
                if(isPortrait())
                {
                    sp_gust.axisY.tickCount = chartview.portraitYticks
                    if(root.forecast === 12)
                    {
                        sp_gust.axisX.tickCount = 5
                        sp_dir.axisXTop.tickCount = 5
                    }
                    else if(root.forecast === 6)
                    {
                        sp_gust.axisX.tickCount = 4
                        sp_dir.axisXTop.tickCount = 4
                    }
                    else
                    {
                        sp_gust.axisX.tickCount = 5 + root.forecast/3
                        sp_dir.axisXTop.tickCount = 5 + root.forecast/3
                    }
                }
                else
                {
                    sp_gust.axisY.tickCount = chartview.landscapeYticks
                    if(root.forecast === 12)
                    {
                        sp_gust.axisX.tickCount = 13
                        sp_dir.axisXTop.tickCount = 13
                    }
                    else if(root.forecast === 6)
                    {
                        sp_gust.axisX.tickCount = 7
                        sp_dir.axisXTop.tickCount = 7
                    }
                    else if(root.forecast === 3)
                    {
                        sp_gust.axisX.tickCount = 6
                        sp_dir.axisXTop.tickCount = 6
                    }
                    else
                    {
                        sp_gust.axisX.tickCount = 7
                    }
                }
            }

            function resetMeasures(start, stop, maxwind, maxgust)
            {
                console.log("resetMeasures wind " + maxwind + " "+ maxgust )
                sp_wind.clear()
                sp_gust.clear()
                sp_dir.clear()
                sp_wind_landscape.clear()
                sp_gust_landscape.clear()
                sp_dir_landscape.clear()
                sp_wind_forecast.clear()
                sp_gust_forecast.clear()
                sp_dir_forecast.clear()

                if(maxwind<0 || maxgust<0)
                    return;

                var max = Math.max(10, Math.floor(maxgust/5+1)*5)

                sp_gust.axisX.min = start
                sp_gust.axisX.max = stop
                sp_gust.axisY.max = max
                sp_gust.color = getWindColor(maxgust)
                chartview.portraitYticks = max<=20 ? max+1 : Math.floor(max/5)+1
                chartview.landscapeYticks = Math.floor(max/5)+1
                setYticks()

                sp_gust_landscape.axisX.min = start
                sp_gust_landscape.axisX.max = stop
                sp_gust_landscape.axisY.max = max
                sp_gust_landscape.color = getWindColor(maxgust)

                sp_wind.axisX.min = start
                sp_wind.axisX.max = stop
                sp_wind.axisY.max = max
                sp_wind.color = getWindColor(maxwind)

                sp_wind_landscape.axisX.min = start
                sp_wind_landscape.axisX.max = stop
                sp_wind_landscape.axisY.max = max
                sp_wind_landscape.color = getWindColor(maxwind)

                sp_wind_forecast.axisX.min = start
                sp_wind_forecast.axisX.max = stop
                sp_wind_forecast.axisY.max = max
                sp_wind_forecast.color = getWindColor(maxwind)

                sp_gust_forecast.axisX.min = start
                sp_gust_forecast.axisX.max = stop
                sp_gust_forecast.axisY.max = max
                sp_gust_forecast.color = getWindColor(maxgust)

                sp_dir.axisXTop.min = start
                sp_dir.axisXTop.max = stop
                sp_dir_forecast.axisX.min = start
                sp_dir_forecast.axisX.max = stop
                sp_dir_landscape.axisXTop.min = start
                sp_dir_landscape.axisXTop.max = stop
            }

            function addMeasure(datetime, wind, gust, direction)
            {
                sp_wind.append(datetime, wind)
                sp_gust.append(datetime, gust)
                if(direction !== -256)
                    sp_dir.append(datetime, direction)
            }

            function addMeasureLandscape(datetime, wind, gust, direction)
            {
                sp_wind_landscape.append(datetime, wind)
                sp_gust_landscape.append(datetime, gust)
                if(direction !== -256)
                    sp_dir_landscape.append(datetime, direction)
            }

            function addForecast(datetime, wind, gust, direction)
            {
                sp_wind_forecast.append(datetime, wind)
                sp_gust_forecast.append(datetime, gust)
                if(direction !== -256)
                    sp_dir_forecast.append(datetime, direction)
            }

            PinchArea {
                anchors.fill: parent
                pinch.target: chartview
                pinch.dragAxis: Pinch.XAxis
            }
        }

    }



    function toMsecsSinceEpoch(date)
    {
        return date.getTime();
    }
}
