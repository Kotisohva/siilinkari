// Copyright (C) 2019 Pasi Kaukinen

// This file is part of Siilinkarin tuulet.

// Siilinkarin tuulet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Siilinkarin tuulet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Siilinkarin tuulet.  If not, see <https://www.gnu.org/licenses/>.

import QtQuick
import QtQuick.Controls
import QtQuick.Controls.Material

Page {
    width: root.width
    height: root.height
    clip: true
    header: Label {
        text: qsTr("ÄÄRIARVOT")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    FlickablePage {
        ListItem {
            id: max_gust
            text1: qsTr("Kovin puuska")
            text2: app.max_gust<0 ? "" : " "+app.max_gust.toFixed(1)+" m/s"
            color1: Material.foreground
            color2: getWindColor(app.max_gust)
            textd: app.max_gust<0 ? "" : toCustom(app.max_gust_time)
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.topMargin: isPortrait() ? height * 0.75 : height * 0.5
        }
        ListItem {
            id: max_speed
            text1: qsTr("Keskituuli")
            text2: app.max_speed<0 ? "" : " "+app.max_speed.toFixed(1)+" m/s"
            color1: Material.foreground
            color2: getWindColor(app.max_speed)
            textd: app.max_speed<0 ? "" : toCustom(app.max_speed_time)
            anchors.left: parent.left
            anchors.top: max_gust.bottom
            anchors.topMargin: height*0.15
        }
        ListItem {
            id: temp_min
            text1: qsTr("Kylmintä")
            text2: app.min_temp===-256 ? "" : " "+app.min_temp.toFixed(1)+" °C"
            color1: Material.foreground
            color2: getTempColor(app.min_temp)
            textd: app.min_temp===-256 ? "" : toCustom(app.min_temp_time)
            anchors.left: isPortrait() ? parent.left : max_gust.right
            anchors.top: isPortrait() ? max_speed.bottom : parent.top
            anchors.topMargin: isPortrait() ? height * 0.75 : height * 0.5
        }
        ListItem {
            id: temp_max
            text1: qsTr("Lämpöisintä")
            text2: app.max_temp===-256 || app.max_temp===256 ? "" : " "+app.max_temp.toFixed(1)+" °C"
            color1: Material.foreground
            color2: getTempColor(app.max_temp)
            textd: app.max_temp===-256 || app.max_temp===256 ? "" : toCustom(app.max_temp_time)
            anchors.left: isPortrait() ? parent.left : max_speed.right
            anchors.top: temp_min.bottom
            anchors.topMargin: height*0.15
        }
    }

}
