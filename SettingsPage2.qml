// Copyright (C) 2019 Pasi Kaukinen

// This file is part of Siilinkarin tuulet.

// Siilinkarin tuulet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Siilinkarin tuulet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Siilinkarin tuulet.  If not, see <https://www.gnu.org/licenses/>.

import QtQuick
import QtQuick.Controls

Page {
    width: root.width
    height: root.height
    header: Label {
        id: label_header
        text: qsTr("ASETUKSET   2/3")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    Label {
        id: wind_label
        text: qsTr("Tuulen väritys")
        font.pixelSize: height * 0.7
        padding: 10
        width: parent.width * 0.4
        height: parent.height * 0.05
        anchors.top: parent.top
        anchors.topMargin: isPortrait() ? 20 : 5
        anchors.left: parent.left
        verticalAlignment: Text.AlignVCenter
    }

    HueSlider {
        id: wind_min
        key: "hue_wind_min"
        label: qsTr("Sininen")
        value: root.hue_wind_min
        color: getColor(240/360)
        anchors.top: wind_label.bottom
        anchors.left: parent.left
    }
    HueSlider {
        id: wind_mid1
        key: "hue_wind_mid1"
        label: qsTr("Vihreä")
        color: getColor(120/360)
        value: root.hue_wind_mid1
        anchors.top: wind_min.bottom
        anchors.left: parent.left
    }
    HueSlider {
        id: wind_mid2
        key: "hue_wind_mid2"
        label: qsTr("Keltainen")
        color: getColor(60/360)
        value: root.hue_wind_mid2
        anchors.top: wind_mid1.bottom
        anchors.left: parent.left
    }
    HueSlider {
        id: wind_mid3
        key: "hue_wind_mid3"
        label: qsTr("Oranssi")
        color: getColor(30/360)
        value: root.hue_wind_mid3
        anchors.top: wind_mid2.bottom
        anchors.left: parent.left
    }
    HueSlider {
        id: wind_mid4
        key: "hue_wind_mid4"
        label: qsTr("Punainen")
        color: getColor(0/360)
        value: root.hue_wind_mid4
        anchors.top: wind_mid3.bottom
        anchors.left: parent.left
    }
    HueSlider {
        id: wind_max
        key: "hue_wind_max"
        label: qsTr("Violetti")
        color: getColor(270/360)
        value: root.hue_wind_max
        anchors.top: wind_mid4.bottom
        anchors.left: parent.left
    }

    Image {
        id: settings_background_image
        width: parent.width
        height: parent.height * 0.15
        anchors.top: wind_max.bottom
        opacity: 0.3
        fillMode: Image.PreserveAspectFit
        source: "images/wind.png"
    }

    Text {
       id: testSingle
       text: "<b>"+root.hue_wind_test.toFixed(1)+ "</b> m/s"
       color: getWindColor(root.hue_wind_test)
       width: parent.width
       height: parent.height * 0.1
       anchors.top: settings_background_image.bottom
       anchors.leftMargin: parent.width * 0.1
       horizontalAlignment: Text.AlignHCenter
       verticalAlignment: Text.AlignVCenter
       fontSizeMode: Text.Fit
       font.pixelSize: height
    }

    ValueSlider {
       value: root.hue_wind_test
       anchors.top: testSingle.bottom
       from: 0
       to: 30
       value_key: "hue_wind_test"
    }
}


