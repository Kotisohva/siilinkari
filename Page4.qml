// Copyright (C) 2019 Pasi Kaukinen

// This file is part of Siilinkarin tuulet.

// Siilinkarin tuulet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Siilinkarin tuulet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Siilinkarin tuulet.  If not, see <https://www.gnu.org/licenses/>.

import QtQuick
import QtQuick.Controls
import QtCharts
import QtQuick.Controls.Material

Page {
    width: root.width
    height: root.height
    clip: true
    header: Label {
        text: qsTr("LÄMPÖKÄYRÄ")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
        bottomPadding: -20
    }

    FlickablePage {
        ChartView {
            id: chartview
            objectName: "tempchartview"
            title: ""
            anchors.fill: parent
            antialiasing: true
            backgroundColor: "transparent"
            backgroundRoundness: 0
            legend.visible: false

            animationDuration: 500
            animationOptions: ChartView.SeriesAnimations

            property int portraitYticks: 11
            property int landscapeYticks: 6

            onWidthChanged: { setYticks() }

            Behavior on scale { NumberAnimation { duration: 200 } }

            SplineSeries {
                id: sp_temp
                visible: root.isPortrait()
                width: 3
                capStyle: Qt.RoundCap
                style: Qt.SolidLine
                color: "red"

                axisX: DateTimeAxis {
                    format: "hh:mm"
                    labelsColor: dark() ? "#E0E0E0" : "#404040"
                    gridLineColor: dark() ? "#404040" : "#E0E0E0"
                }
                axisY: ValueAxis {
                    labelFormat: "%2i"
                    labelsColor: dark() ? "#E0E0E0" : "#404040"
                    gridLineColor: dark() ? "#404040" : "#E0E0E0"
                    min: 0
                }
            }

            SplineSeries {
                id: sp_temp_landscape
                visible: root.isLandscape()
                width: 3
                capStyle: Qt.RoundCap
                style: Qt.SolidLine
                color: "red"

                axisX: DateTimeAxis {
                    visible: false
                }
                axisY: ValueAxis {
                    visible: false
                    min: 0
                }
            }

            SplineSeries {
                id: sp_temp_forecast
                name: "Lämpötila ennuste"
                width: 3
                capStyle: Qt.RoundCap
                style: Qt.DotLine
                color: "grey"

                axisX: DateTimeAxis {
                    visible: false
                }
            }

            function setYticks()
            {
                if(isPortrait())
                {
                    sp_temp.axisY.tickCount = chartview.portraitYticks
                    if(root.forecast === 12)
                        sp_temp.axisX.tickCount = 5
                    else if(root.forecast === 6)
                        sp_temp.axisX.tickCount = 4
                    else
                        sp_temp.axisX.tickCount = 5 + root.forecast/3
                }
                else
                {
                    sp_temp.axisY.tickCount = chartview.landscapeYticks
                    if(root.forecast === 12)
                        sp_temp.axisX.tickCount = 13
                    else if(root.forecast === 6)
                        sp_temp.axisX.tickCount = 7
                    else  if(root.forecast === 3)
                        sp_temp.axisX.tickCount = 6
                    else
                        sp_temp.axisX.tickCount = 7
                }
            }

            function resetMeasures(start, stop, mintemp, maxtemp)
            {
                console.log("resetMeasures temp " + mintemp +" "+ maxtemp )
                sp_temp_forecast.clear()
                sp_temp_landscape.clear()
                sp_temp.clear()

                if(mintemp===-256 || maxtemp===-256)
                    return

                var max = Math.max(0, Math.floor(maxtemp/5+1)*5)
                var min = Math.min(0, Math.floor(mintemp/5)*5)
                var tot = max - min

                sp_temp.axisX.min = start
                sp_temp.axisX.max = stop
                sp_temp.axisY.max = max
                sp_temp.axisY.min = min

                sp_temp_landscape.axisX.min = start
                sp_temp_landscape.axisX.max = stop
                sp_temp_landscape.axisY.max = max
                sp_temp_landscape.axisY.min = min
                sp_temp_landscape.color = getTempColor((mintemp+maxtemp)*0.5)

                chartview.portraitYticks = tot<=20 ? tot+1 : Math.floor(tot/5)+1
                chartview.landscapeYticks = tot<=10 ? tot+1 : Math.floor(tot/5)+1
                setYticks()
                sp_temp.color = getTempColor((mintemp+maxtemp)*0.5)

                sp_temp_forecast.axisX.min = start
                sp_temp_forecast.axisX.max = stop
                sp_temp_forecast.axisY.max = max
                sp_temp_forecast.axisY.min = min
                sp_temp_forecast.color = getTempColor((mintemp+maxtemp)*0.5)
            }

            function addMeasure(datetime, temp)
            {
                sp_temp.append(datetime, temp)
            }

            function addMeasureLandscape(datetime, temp)
            {
                sp_temp_landscape.append(datetime, temp)
            }

            function addForecast(datetime, temp)
            {
                sp_temp_forecast.append(datetime, temp)
            }

            PinchArea {
                anchors.fill: parent
                pinch.target: chartview
                pinch.dragAxis: Pinch.XAxis
            }
        }

    }



    function toMsecsSinceEpoch(date)
    {
        return date.getTime();
    }
}
